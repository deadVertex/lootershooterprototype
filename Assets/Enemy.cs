﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Enemy : NetworkBehaviour {

    private Transform targetTransform;
    private UnityEngine.AI.NavMeshAgent agent;
    bool isChasing = false;
    float timeUntilLeap = 5.0f;
    float leapTime;
    float leapSpeed = 15.0f;
    float defaultSpeed;
    float leapAngularSpeed = 80.0f;
    float defaultAngularSpeed;
    bool isLeaping = false;
    public bool canLeap = true;

	// Use this for initialization
	void Start () {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        defaultSpeed = agent.speed;
        defaultAngularSpeed = agent.angularSpeed;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (!isServer)
        {
            return;
        }

        if (isChasing)
        {
            if (canLeap)
            {
                if (isLeaping)
                {
                    leapTime -= Time.deltaTime;
                    if (leapTime <= 0.0f)
                    {
                        isLeaping = false;
                        agent.speed = defaultSpeed;
                        agent.angularSpeed = defaultAngularSpeed;
                    }
                }
                else
                {
                    timeUntilLeap -= Time.deltaTime;
                    if (timeUntilLeap <= 0.0f)
                    {
                        timeUntilLeap = Random.Range(3.5f, 6.0f);
                        //timeUntilLeap = 5.0f;
                        leapTime = 1.5f;
                        agent.speed = leapSpeed;
                        agent.angularSpeed = leapAngularSpeed;
                        isLeaping = true;
                    }
                }
            }

            agent.SetDestination(targetTransform.position);
        }
        Animator animator = GetComponentInChildren<Animator>();
        if (animator != null)
        {
            animator.SetFloat("vertical", agent.velocity.magnitude);
            //animator.SetFloat("horizontal", agent.velocity.x);
        }
    }

    public void SetTarget(GameObject target)
    {
        if (!isServer)
        {
            return;
        }

        if (target)
        {
            targetTransform = target.transform;
            isChasing = true;
            agent.enabled = true;
        }
        else
        {
            isChasing = false;
            agent.enabled = false;
        }
    }
}
