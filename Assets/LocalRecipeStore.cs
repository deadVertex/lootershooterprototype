﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Assertions;

public class LocalRecipeStore : NetworkBehaviour
{
    [SerializeField]
    private RecipeId[] m_KnownRecipes;

    private bool[] m_Recipes = new bool[(int)RecipeId.MaxRecipes];
    private SyncListUInt m_RecipeSyncList = new SyncListUInt();

    void Start()
    {
        foreach (RecipeId recipe in m_KnownRecipes)
        {
            m_Recipes[(int)recipe] = true;
        }

        if (isClient)
        {
            m_RecipeSyncList.Callback = OnSyncListChange;
        }
    }

    void OnSyncListChange(SyncListUInt.Operation op, int index)
    {
        switch (op)
        {
            case SyncList<uint>.Operation.OP_ADD:
                uint recipeId = m_RecipeSyncList[index];
                m_Recipes[recipeId] = true;
                break;
            default:
                // Only support adding new recipes to the list
                break;
        }
    }

    [Server]
    public void sv_AddRecipe(RecipeId recipe)
    {
        m_Recipes[(int)recipe] = true;
        m_RecipeSyncList.Add((uint)recipe);
    }

    public bool IsRecipeKnown(RecipeId recipe)
    {
        return m_Recipes[(int)recipe];
    }

}
