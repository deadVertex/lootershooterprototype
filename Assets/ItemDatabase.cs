﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;

public enum ItemType
{
    MeleeWeapon,
    Gun,
    FirstAid,
    Ammo,
    Food,
    Bow,
    Resource,
    BuildingPart,
    Crossbow,
    Blueprint,
};

// NOTE: Ascending order of rarity, rarest should be at the bottom
public enum ItemRarity
{
    Common,
    Uncommon,
    Rare,
    VeryRare,



    // NOTE: All rarity levels must come before this
    MaxRarity,
};

public enum ItemID
{
    None,
    Crowbar,
    Machete,
    Bandage,
    Pistol,
    Ammo_9mm,
    MetalHatchet,
    CanOfBeans,
    Bow,
    StoneArrow,
    RottingWood,
    WoodenClub,
    WoodFoundation,
    WoodPillar,
    WoodWall,
    WoodCeiling,
    WoodStairs,
    WoodStorageBox,
    Crossbow,
    CrossbowBlueprint,
    Akm,



    // NOTE: All item IDs must come before this
    MaxItems,
};

public class ItemDbEntry
{
    public ItemType type;
    public ItemID id;
    public ItemRarity rarity;
    public string name;
    public float damage;
    public float timeBetweenAttacks;
    public float healthRestored;
    public float foodRestored;
    public bool isStackable;
    public int buildingPartIdx;
    public RecipeId recipe;

    public ItemDbEntry()
    {
    }

    public ItemDbEntry(ItemDbEntry item)
    {
        type = item.type;
        id = item.id;
        name = item.name;
        damage = item.damage;
        timeBetweenAttacks = item.timeBetweenAttacks;
        healthRestored = item.healthRestored;
        foodRestored = item.foodRestored;
        isStackable = item.isStackable;
        buildingPartIdx = item.buildingPartIdx;
        recipe = item.recipe;
    }
}

public class ItemDatabase {

    private ItemDbEntry[] items;
    private ArrayList[] itemsByRarity;

    static private ItemDatabase instance = null;

	void Initialize () {
        items = new ItemDbEntry[(int)ItemID.MaxItems];
        itemsByRarity = new ArrayList[(int)ItemRarity.MaxRarity];
        for (int i = 0; i < (int)ItemRarity.MaxRarity; ++i)
        {
            itemsByRarity[i] = new ArrayList();
        }

        ItemDbEntry crowbar = new ItemDbEntry();
        crowbar.type = ItemType.MeleeWeapon;
        crowbar.rarity = ItemRarity.Common;
        crowbar.name = "Rusty Crowbar";
        crowbar.damage = 15;
        crowbar.timeBetweenAttacks = 0.3f;
        crowbar.isStackable = false;
        RegisterItemDbEntry(ItemID.Crowbar, crowbar);

        ItemDbEntry bandage = new ItemDbEntry();
        bandage.type = ItemType.FirstAid;
        bandage.rarity = ItemRarity.Common;
        bandage.name = "Bandage";
        bandage.healthRestored = 20;
        bandage.timeBetweenAttacks = 1.0f;
        bandage.isStackable = true;
        RegisterItemDbEntry(ItemID.Bandage, bandage);

        ItemDbEntry machete = new ItemDbEntry();
        machete.type = ItemType.MeleeWeapon;
        machete.rarity = ItemRarity.Uncommon;
        machete.name = "Machete";
        machete.timeBetweenAttacks = 0.4f;
        machete.damage = 60.0f;
        machete.isStackable = false;
        RegisterItemDbEntry(ItemID.Machete, machete);

        ItemDbEntry pistol = new ItemDbEntry();
        pistol.type = ItemType.Gun;
        pistol.rarity = ItemRarity.Rare;
        pistol.name = "9mm Pistol";
        pistol.damage = 40;
        pistol.timeBetweenAttacks = 0.1f;
        pistol.isStackable = false;
        RegisterItemDbEntry(ItemID.Pistol, pistol);

        ItemDbEntry pistolAmmo = new ItemDbEntry();
        pistolAmmo.type = ItemType.Ammo;
        pistolAmmo.rarity = ItemRarity.Uncommon;
        pistolAmmo.name = "9mm Ammo";
        pistolAmmo.isStackable = true;
        RegisterItemDbEntry(ItemID.Ammo_9mm, pistolAmmo);

        ItemDbEntry metalHatchet = new ItemDbEntry();
        metalHatchet.type = ItemType.MeleeWeapon;
        metalHatchet.rarity = ItemRarity.Uncommon;
        metalHatchet.name = "Metal Hatchet";
        metalHatchet.timeBetweenAttacks = 0.2f;
        metalHatchet.damage = 25.0f;
        metalHatchet.isStackable = false;
        RegisterItemDbEntry(ItemID.MetalHatchet, metalHatchet);

        ItemDbEntry canOfBeans = new ItemDbEntry();
        canOfBeans.type = ItemType.Food;
        canOfBeans.rarity = ItemRarity.Common;
        canOfBeans.name = "Can of Beans";
        canOfBeans.timeBetweenAttacks = 1.0f;
        canOfBeans.foodRestored = 40;
        canOfBeans.isStackable = true;
        RegisterItemDbEntry(ItemID.CanOfBeans, canOfBeans);

        ItemDbEntry bow = new ItemDbEntry();
        bow.type = ItemType.Bow;
        pistol.rarity = ItemRarity.Uncommon;
        bow.name = "Bow";
        bow.damage = 40;
        bow.timeBetweenAttacks = 0.1f;
        bow.isStackable = false;
        RegisterItemDbEntry(ItemID.Bow, bow);

        ItemDbEntry stoneArrow = new ItemDbEntry();
        stoneArrow.type = ItemType.Ammo;
        stoneArrow.rarity = ItemRarity.Uncommon;
        stoneArrow.name = "Stone Arrow";
        stoneArrow.isStackable = true;
        RegisterItemDbEntry(ItemID.StoneArrow, stoneArrow);

        ItemDbEntry rottingWood = new ItemDbEntry();
        rottingWood.type = ItemType.Resource;
        rottingWood.rarity = ItemRarity.Uncommon;
        rottingWood.name = "RottingWood";
        rottingWood.isStackable = true;
        RegisterItemDbEntry(ItemID.RottingWood, rottingWood);

        ItemDbEntry woodenClub = new ItemDbEntry();
        woodenClub.type = ItemType.MeleeWeapon;
        woodenClub.rarity = ItemRarity.Common;
        woodenClub.name = "Wooden Club";
        woodenClub.timeBetweenAttacks = 0.3f;
        woodenClub.damage = 15.0f;
        woodenClub.isStackable = false;
        RegisterItemDbEntry(ItemID.WoodenClub, woodenClub);

        ItemDbEntry woodFoundation = new ItemDbEntry();
        woodFoundation.type = ItemType.BuildingPart;
        woodFoundation.rarity = ItemRarity.Common;
        woodFoundation.name = "Wood Foundation";
        woodFoundation.isStackable = true;
        woodFoundation.buildingPartIdx = 0;
        RegisterItemDbEntry(ItemID.WoodFoundation, woodFoundation);

        ItemDbEntry woodPillar = new ItemDbEntry();
        woodPillar.type = ItemType.BuildingPart;
        woodPillar.rarity = ItemRarity.Common;
        woodPillar.name = "Wood Pillar";
        woodPillar.isStackable = true;
        woodPillar.buildingPartIdx = 1;
        RegisterItemDbEntry(ItemID.WoodPillar, woodPillar);

        ItemDbEntry woodWall = new ItemDbEntry();
        woodWall.type = ItemType.BuildingPart;
        woodWall.rarity = ItemRarity.Common;
        woodWall.name = "Wood Wall";
        woodWall.isStackable = true;
        woodWall.buildingPartIdx = 2;
        RegisterItemDbEntry(ItemID.WoodWall, woodWall);

        ItemDbEntry woodCeiling = new ItemDbEntry();
        woodCeiling.type = ItemType.BuildingPart;
        woodCeiling.rarity = ItemRarity.Common;
        woodCeiling.name = "Wood Ceiling";
        woodCeiling.isStackable = true;
        woodCeiling.buildingPartIdx = 3;
        RegisterItemDbEntry(ItemID.WoodCeiling, woodCeiling);

        ItemDbEntry woodStairs = new ItemDbEntry();
        woodStairs.type = ItemType.BuildingPart;
        woodStairs.rarity = ItemRarity.Common;
        woodStairs.name = "Wood Stairs";
        woodStairs.isStackable = true;
        woodStairs.buildingPartIdx = 4;
        RegisterItemDbEntry(ItemID.WoodStairs, woodStairs);

        ItemDbEntry woodStorageBox = new ItemDbEntry();
        woodStorageBox.type = ItemType.BuildingPart;
        woodStorageBox.rarity = ItemRarity.Common;
        woodStorageBox.name = "Wood Storage Box";
        woodStorageBox.isStackable = true;
        woodStorageBox.buildingPartIdx = 5;
        RegisterItemDbEntry(ItemID.WoodStorageBox, woodStorageBox);

        ItemDbEntry crossbow = new ItemDbEntry();
        crossbow.type = ItemType.Crossbow;
        crossbow.rarity = ItemRarity.Uncommon;
        crossbow.name = "Crossbow";
        crossbow.isStackable = false;
        crossbow.damage = 40;
        crossbow.timeBetweenAttacks = 0.1f;
        RegisterItemDbEntry(ItemID.Crossbow, crossbow);

        ItemDbEntry crossbowBlueprint = new ItemDbEntry();
        crossbowBlueprint.type = ItemType.Blueprint;
        crossbowBlueprint.rarity = ItemRarity.Uncommon;
        crossbowBlueprint.name = "Crossbow Blueprint";
        crossbowBlueprint.isStackable = false;
        crossbowBlueprint.recipe = RecipeId.Crossbow;
        RegisterItemDbEntry(ItemID.CrossbowBlueprint, crossbowBlueprint);

        ItemDbEntry akm = new ItemDbEntry();
        akm.type = ItemType.Gun;
        akm.rarity = ItemRarity.Rare;
        akm.name = "AKM";
        akm.damage = 60;
        akm.timeBetweenAttacks = 0.1f;
        akm.isStackable = false;
        RegisterItemDbEntry(ItemID.Akm, akm);
	}

    public void RegisterItemDbEntry(ItemID id, ItemDbEntry item)
    {
        item.id = id;
        items[(int)id] = item;
        itemsByRarity[(int)item.rarity].Add(id);
    }

    public ItemDbEntry GetItemById(ItemID id)
    {
        ItemDbEntry result = items[(int)id];
        Assert.IsNotNull(result);
        return result;
    }

    public ItemDbEntry GetRandomItemByRarity(ItemRarity rarity)
    {
        ArrayList list = itemsByRarity[(int)rarity];
        if (list.Count > 0)
        {
            int idx = Random.Range(0, list.Count);
            ItemID id = (ItemID)list[idx];
            return GetItemById(id);
        }
        else
        {
            return null;
        }
    }

    public static ItemDatabase GetInstance()
    {
        if (instance == null)
        {
            instance = new ItemDatabase();
            instance.Initialize();
        }
        return instance;
    }
}
