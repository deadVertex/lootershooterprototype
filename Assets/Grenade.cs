﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Grenade : NetworkBehaviour {

    [SyncVar]
    public float timeRemaining;
    public GameObject explosionPrefab;
	
	// Update is called once per frame
	void Update () {
        timeRemaining -= Time.deltaTime;		

        if (timeRemaining <= 0.0f)
        {
            Detonate();
        }
	}

    void Detonate()
    {
        float maxRange = 5.0f; // TODO: Don't hardcode radius
        float maxDamage = 80.0f;
        RaycastHit[] hits = Physics.SphereCastAll(transform.position, maxRange, new Vector3(0.0f, 0.001f, 0.0f));
        foreach (RaycastHit hit in hits)
        {
            Killable killable = hit.collider.GetComponent<Killable>();
            if (killable != null)
            {
                Vector3 direction = Vector3.Normalize(hit.transform.position - transform.position);
                RaycastHit rayHit;
                if (Physics.Raycast(transform.position, direction, out rayHit, maxRange))
                {
                    float t = rayHit.distance / maxRange; // TODO: Don't use linear damage curve
                    float damage = Mathf.Round((1.0f - t) * maxDamage);
                    killable.ReceiveDamage(damage);
                }
            }
        }
        GameObject explosion = (GameObject)Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        Destroy(explosion, 5.0f);
        Destroy(gameObject);
    }
}
