﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

// Duplicate of GrenadeThrowAttack
public class ArrowShootAttack : NetworkBehaviour {

    public GameObject arrowPrefab;
    private GameObject m_target;
    private float lastThrowTime;
    static float arrowVelocity = 3000.0f; // Copied from player
    private Enemy m_EnemyComponent;
    private UnityEngine.AI.NavMeshAgent m_NavMeshAgent;

	void Start ()
    {
        Killable killable = GetComponentInParent<Killable>();
        killable.m_OnDeath = OnDeath;
        m_EnemyComponent = GetComponent<Enemy>();
        m_NavMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
	}

    void OnDeath()
    {
        enabled = false;
    }

    // Firing solution from AI for games book
    static bool CalculateFiringSolution(Vector3 start, Vector3 end, float velocity, Vector3 gravity, out Vector3 result)
    {
        Vector3 delta = end - start;
        float a = Vector3.Dot(gravity, gravity);
        float b = -4 * (Vector3.Dot(Physics.gravity, delta) + velocity * velocity);
        float c = 4 * Vector3.Dot(delta, delta);

        if ((4 * a * c) > b * b)
        {
            Debug.Log("No solution available");
            result = Vector3.zero;
            return false;
        }

        float t0 = Mathf.Sqrt((-b + Mathf.Sqrt(b * b - 4 * a * c)) / (2 * a));
        float t1 = Mathf.Sqrt((-b - Mathf.Sqrt(b * b - 4 * a * c)) / (2 * a));
        float t = 0.0f;
        if (t0 < 0.0f)
        {
            if (t1 < 0.0f)
            {
                Debug.Log("No valid times");
                result = Vector3.zero;
                return false;
            }
            else
            {
                t = t1;
            }
        }
        else
        {
            if (t1 < 0.0f)
            {
                t = t0;
            }
            else
            {
                t = Mathf.Min(t0, t1);
            }
        }
        if (t <= 0.0f)
        {
            t = Time.deltaTime;
        }

        result = ((2 * delta - gravity * t * t) / (2 * velocity * t)) * velocity;
        return true;
    }
	
	void FixedUpdate ()
    {
        if (!isServer)
        {
            return;
        }

        if (m_target)
        {
            float range = 200.0f;
            if (VisionSystem.LineOfSightTest(m_target, transform.position, range))
            {
                m_EnemyComponent.SetTarget(null);
                Vector3 v = m_target.transform.position - transform.position;
                float d = -Mathf.Atan2(-v.x, v.z) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.AngleAxis(d, Vector3.up);
                if ((Time.time - lastThrowTime > 2.0f) && (m_NavMeshAgent.velocity.magnitude < 0.2f))
                {
                    Vector3 origin = transform.position + Vector3.up * 0.8f;
                    Vector3 targetPosition = m_target.transform.position + new Vector3(0.0f, 0.8f, 0.0f);
                    Vector3 delta = targetPosition - origin;
                    Vector3 direction = delta.normalized;
                    float distanceToTarget = delta.magnitude;
                    // TODO: Check if target is in range before firing arrow

                    CharacterController characterController = m_target.GetComponentInParent<CharacterController>();
                    Vector3 velocity = new Vector3(characterController.velocity.x, 0.0f, characterController.velocity.z);
                    float travelTime = distanceToTarget / arrowVelocity;
                    targetPosition += velocity * travelTime; // Stupid prediction
                    Vector3 firingVector = Vector3.zero;
                    if (CalculateFiringSolution(origin, targetPosition, arrowVelocity, Physics.gravity, out firingVector))
                    {
                        float mag = firingVector.magnitude;
                        if (mag > 0.0f)
                        {
                            Player.SpawnArrow(origin, firingVector.normalized, arrowVelocity, arrowPrefab, gameObject);
                        }
                    }
                    lastThrowTime = Time.time;
                }
            }
            else
            {
                m_EnemyComponent.SetTarget(m_target);
            }
        }
    }

    public void SetTarget(GameObject target)
    {
        m_target = target;
    }
}