﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletManager
{
    struct BulletState
    {
        public Vector3 position;
        public Vector3 velocity;
    };

    public struct BulletImpactEvent
    {
        public Vector3 position;
        public Vector3 normal;
    };

    const uint m_MaxBullets = 4096;
    const uint m_MaxRayCastResults = 8;

    BulletState[] m_Bullets = new BulletState[m_MaxBullets];
    uint m_BulletCount = 0;
    RaycastHit[] m_RaycastResults = new RaycastHit[m_MaxRayCastResults];

    public BulletImpactEvent[] m_ImpactEvents = new BulletImpactEvent[m_MaxBullets];
    public uint m_ImpactEventCount = 0;

    float m_MaxVelocity;

    public void FixedUpdate(float dt)
    {
        m_ImpactEventCount = 0;
        uint i = 0;
        while (i < m_BulletCount)
        {
            BulletState bullet = m_Bullets[i];
            Vector3 dir = bullet.velocity.normalized;
            float dist = bullet.velocity.magnitude * dt;
            int resultCount = Physics.RaycastNonAlloc(bullet.position, dir, m_RaycastResults, dist);
            if (resultCount > 0)
            {
                // Update velocity
                RaycastHit hitResult = m_RaycastResults[0];
                bullet.position = hitResult.point;
                bullet.velocity = Vector3.zero;
                m_Bullets[i] = bullet;
                BulletImpactEvent impactEvent = m_ImpactEvents[m_ImpactEventCount];
                impactEvent.position = hitResult.point;
                impactEvent.normal = hitResult.normal;
                m_ImpactEvents[m_ImpactEventCount++] = impactEvent;
                RemoveBulletAtIndex(i);
                // Repeat update for same value of i
            }
            else
            {
                bullet.position += bullet.velocity * dt;
                bullet.velocity = CalculateVelocity(Vector3.zero, bullet.velocity, dt);
                m_Bullets[i] = bullet;
                i++;
            }
        }
    }

    public void Update(float dt)
    {
        for (uint i = 0; i < m_BulletCount; ++i)
        {
            BulletState bullet = m_Bullets[i];
            Vector3 endPosition = bullet.position + bullet.velocity * dt;
            float mag = bullet.velocity.magnitude;
            Color colour = Color.Lerp(Color.red, Color.green, mag / m_MaxVelocity);
            Debug.DrawLine(bullet.position, endPosition, colour, 10.0f, false);
        }
    }

    static Vector3 CalculateVelocity(Vector3 acceleration, Vector3 velocity, float dt)
    {
        acceleration += Physics.gravity;
        float friction = 0.05f;
        acceleration += (velocity * friction);
        Vector3 result = acceleration * dt + velocity;
        return result;
    }

    public void SpawnBullet(Vector3 origin, Vector3 direction, float acceleration, float dt)
    {
        BulletState bullet = m_Bullets[m_BulletCount];
        bullet.position = origin;
        bullet.velocity = CalculateVelocity(direction * acceleration, Vector3.zero, dt);
        m_Bullets[m_BulletCount++] = bullet;
        float mag = bullet.velocity.magnitude;
        if (mag > m_MaxVelocity)
        {
            m_MaxVelocity = mag;
        }
    }

    void RemoveBulletAtIndex(uint idx)
    {
        if (m_BulletCount > 0)
        {
            uint last = m_BulletCount - 1;
            if (last != idx)
            {
                m_Bullets[idx] = m_Bullets[last];
            }
            m_BulletCount--;
        }
    }
}
