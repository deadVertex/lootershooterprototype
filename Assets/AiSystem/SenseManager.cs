﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisionSystem
{
    static VisionSystem m_Instance;

    public delegate void OnEntityOfInterestSighted(GameObject entity);
    public struct Sensor
    {
        public Transform transform;
        public float range;
        public float fovCosine;
        public OnEntityOfInterestSighted onEntityOfInterestSighted;
    };

    private ArrayList m_Sensors = new ArrayList();
    private ArrayList m_EntitiesOfInterest = new ArrayList();

    public void RegisterEntityOfInterest(GameObject gameObject)
    {
        m_EntitiesOfInterest.Add(gameObject);
    }

    public void RegisterSensor(Transform transform, float range, float fov, OnEntityOfInterestSighted callback)

    {
        Sensor sensor = new Sensor();
        sensor.transform = transform;
        sensor.range = range;
        sensor.fovCosine = Mathf.Cos(Mathf.Deg2Rad * fov);
        sensor.onEntityOfInterestSighted = callback;
        m_Sensors.Add(sensor);
    }
    public void DrawVisionFrustum(Transform transform, float range, float fovCosine)
    {
        float fov = Mathf.Acos(fovCosine) * Mathf.Rad2Deg;
        Vector3 origin = transform.position;
        Vector3 a = transform.rotation * Quaternion.Euler(0, fov, 0) * Vector3.forward;
        Vector3 b = transform.rotation * Quaternion.Euler(0, -fov, 0) * Vector3.forward;
        Debug.DrawLine(transform.position, transform.position + a * range, Color.red, 1.0f, false);
        Debug.DrawLine(transform.position, transform.position + b * range, Color.red, 1.0f, false);
    }

    public void FixedUpdate()
    {
        // Foreach sensor
        foreach (Sensor sensor in m_Sensors)
        {
            //Debug.Log("Processing sensor: " + sensor.transform.gameObject);
            Vector3 origin = sensor.transform.position;
            Vector3 direction = sensor.transform.rotation * Vector3.forward;
            DrawVisionFrustum(sensor.transform, sensor.range, sensor.fovCosine);

            // Find all things of interest that are in range
            ArrayList entitiesInRange = new ArrayList();
            foreach (GameObject entity in m_EntitiesOfInterest)
            {
                float distance = Vector3.Distance(origin, entity.transform.position);
                if (distance < sensor.range)
                {
                    entitiesInRange.Add(entity);
                }
            }

            // Check if they are within the view fov
            ArrayList entitiesInFov = new ArrayList();
            foreach (GameObject entity in entitiesInRange)
            {
                Vector3 forward = Vector3.Normalize(entity.transform.position - origin);
                float dot = Vector3.Dot(direction, forward);
                if (dot > sensor.fovCosine)
                {
                    entitiesInFov.Add(entity);
                    //Debug.DrawLine(origin, entity.transform.position, Color.green, Time.fixedDeltaTime, false);
                }
            }

            // Check for line of sight
            foreach (GameObject entity in entitiesInFov)
            {
                if (LineOfSightTest(entity, origin, sensor.range))
                {
                    sensor.onEntityOfInterestSighted(entity);
                }
            }
        }
    }

    public static bool LineOfSightTest(GameObject entity, Vector3 origin, float range)
    {
        Vector3 entityDirection = Vector3.Normalize(entity.transform.position - origin);
        RaycastHit raycastResult;
        if (Physics.Raycast(origin, entityDirection, out raycastResult, range))
        {
            //Debug.DrawLine(origin, raycastResult.point, Color.blue, Time.fixedDeltaTime, false);
            if (raycastResult.collider.gameObject == entity)
            {
                return true;
            }
        }
        return false;
    }

    public static VisionSystem GetInstance()
    {
        if (m_Instance == null)
        {
            m_Instance = new VisionSystem();
        }

        return m_Instance;
    }
};

public class SenseManager : MonoBehaviour
{
    void Start()
    {
    }

    void FixedUpdate()
    {
        VisionSystem.GetInstance().FixedUpdate();
    }
}
