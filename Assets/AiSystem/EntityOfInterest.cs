﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityOfInterest : MonoBehaviour
{
	// Use this for initialization
	void Start () {
        VisionSystem.GetInstance().RegisterEntityOfInterest(gameObject);		
	}
}
