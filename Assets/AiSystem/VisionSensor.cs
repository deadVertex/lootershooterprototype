﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisionSensor : UnityEngine.Networking.NetworkBehaviour {

	// Use this for initialization
	void Start () {
        if (isServer)
        {
            VisionSystem.GetInstance().RegisterSensor(transform, 100.0f, 60.0f, OnEntityOfInterestSighted);
        }
	}

    void OnEntityOfInterestSighted(GameObject entityOfInterest)
    {
        //Debug.Log("Spotted " + entityOfInterest);

        ArrowShootAttack attack = GetComponent<ArrowShootAttack>();
        if (attack)
        {
            attack.SetTarget(entityOfInterest);
        }
        else
        {
            Enemy enemy = GetComponent<Enemy>();
            enemy.SetTarget(entityOfInterest);
        }
    }
}
