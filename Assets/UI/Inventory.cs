﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using UnityEngine.Networking;

public interface IActionBarObserver
{
    void OnSetActionBarSelection(int slot);
};

public class Inventory : Container {

    private int selectedActionBarSlot = 0;

    private IActionBarObserver m_ActionBarObserver;

	void Start ()
    {
        Initialize(InventoryView.totalSlotCount, true);
	}

    public void SetActionBarObserver(IActionBarObserver observer)
    {
        m_ActionBarObserver = observer;
    }

    public int SelectedActionBarSlot()
    {
        return selectedActionBarSlot;
    }

    private void SetSelectedActionBarSlotInternal(int slot)
    {
        Assert.IsTrue(slot < InventoryView.actionBarSlotCount);
        selectedActionBarSlot = slot;
        if (m_ActionBarObserver != null)
        {
            m_ActionBarObserver.OnSetActionBarSelection(slot);
        }
    }

    public void sv_SetSelectedActionBarSlot(int slot)
    {
        SetSelectedActionBarSlotInternal(slot);
        if (!isLocalPlayer)
        {
            RpcOnSetActionBarSelection(slot);
        }
    }

    [ClientRpc]
    private void RpcOnSetActionBarSelection(int slot)
    {
        SetSelectedActionBarSlotInternal(slot);
    }

    public ItemSlot GetSelectedItemFromActionBar()
    {
        return GetItemInSlot(InventoryView.inventorySlotCount + selectedActionBarSlot);
    }

    [Server]
    public void sv_ConsumeActiveActionBarSlot()
    {
        sv_ConsumeSlot(InventoryView.inventorySlotCount + selectedActionBarSlot);
    }

}
