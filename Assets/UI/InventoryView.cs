﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class InventoryView : MonoBehaviour, IActionBarObserver
{
    public class ContainerView : IContainerObserver
    {
        public InventoryView m_InventoryView;
        public bool m_IsContainerView;

        public void OnSetItemInSlot(ItemSlot item, int slot)
        {
            if (m_IsContainerView)
            {
                m_InventoryView.SetItemInContainerSlot(item, slot);
            }
            else
            {
                m_InventoryView.OnSetItemInSlot(item, slot);
            }
        }

        public void ClearAllContainerSlots()
        {
            for (int i = 0; i < InventoryView.containerSlotCount; ++i)
            {
                ItemSlot item;
                item.id = ItemID.None;
                item.quantity = 0;
                m_InventoryView.SetItemInContainerSlot(item, i);
            }
        }
    };

    public static int inventorySlotCount = 30;
    public static int actionBarSlotCount = 6;
    public static int totalSlotCount = inventorySlotCount + actionBarSlotCount;
    public static int containerSlotCount = totalSlotCount;

    public GameObject slotPrefab;
    public GameObject craftingSlotPrefab;
    public ContainerView m_ContainerView;
    public ContainerView m_InventoryContainerView;

    private GameObject[] slots;
    private GameObject[] craftingSlots;
    private GameObject[] containerSlots;
    private int selectedActionBarSlot;
    private bool isInitialized = false;

    private GameObject m_InventoryPanel;
    private GameObject m_ContainerSlots;
    private GameObject m_CraftingPanel;

	// Use this for initialization
	public void Start () {
        if (!isInitialized)
        {
            slots = new GameObject[totalSlotCount];
            containerSlots = new GameObject[containerSlotCount];

            m_InventoryPanel = GameObject.Find("InventoryCanvas/InventoryPanelBg");
            Assert.IsNotNull(m_InventoryPanel);

            GameObject inventorySlotsObject = GameObject.Find("InventoryCanvas/InventoryPanelBg/InventorySlots");
            Assert.IsNotNull(inventorySlotsObject);

            GameObject actionBarSlotsObject = GameObject.Find("InventoryCanvas/ActionBarSlots");
            Assert.IsNotNull(actionBarSlotsObject);

            GameObject inventoryPanelBg = GameObject.Find("InventoryCanvas/InventoryPanelBg");
            Assert.IsNotNull(inventoryPanelBg);

            m_ContainerSlots = inventoryPanelBg.transform.Find("ContainerSlots").gameObject;
            Assert.IsNotNull(m_ContainerSlots);

            m_CraftingPanel = inventoryPanelBg.transform.Find("CraftingPanel").gameObject;
            Assert.IsNotNull(m_CraftingPanel);

            GameObject craftingSlotsObject = m_CraftingPanel.transform.Find("CraftingSlots").gameObject; 
            Assert.IsNotNull(craftingSlotsObject);

            Assert.IsNotNull(craftingSlotPrefab);

            for (int i = 0; i < inventorySlotCount; ++i)
            {
                GameObject slotInstance = Instantiate(slotPrefab);
                slotInstance.transform.SetParent(inventorySlotsObject.transform);
                slots[i] = slotInstance;
                Slot slotComponent = slotInstance.GetComponent<Slot>();
                slotComponent.index = i;
            }

            for (int i = 0; i < actionBarSlotCount; ++i)
            {
                GameObject slotInstance = Instantiate(slotPrefab);
                slotInstance.transform.SetParent(actionBarSlotsObject.transform);
                slots[i + inventorySlotCount] = slotInstance;
                Slot slotComponent = slotInstance.GetComponent<Slot>();
                slotComponent.index = i + inventorySlotCount;
            }

            for (int i = 0; i < containerSlotCount; ++i)
            {
                GameObject slotInstance = Instantiate(slotPrefab);
                slotInstance.transform.SetParent(m_ContainerSlots.transform);
                containerSlots[i] = slotInstance;
                Slot slotComponent = slotInstance.GetComponent<Slot>();
                slotComponent.index = i;
                slotComponent.isContainer = true;
            }

            craftingSlots = new GameObject[(int)RecipeId.MaxRecipes];
            RecipeDatabase recipeDb = RecipeDatabase.GetInstance();
            ItemDatabase itemDb = ItemDatabase.GetInstance();
            for (RecipeId id = 0; id < RecipeId.MaxRecipes; ++id)
            {
                CraftingRecipe recipe = recipeDb.GetRecipeById(id);
                ItemDbEntry item = itemDb.GetItemById(recipe.result);
                GameObject slotInstance = Instantiate(craftingSlotPrefab);
                slotInstance.transform.SetParent(craftingSlotsObject.transform);
                craftingSlots[(int)id] = slotInstance;
                craftingSlots[(int)id].SetActive(false);
                CraftingSlot slotComponent = slotInstance.GetComponent<CraftingSlot>();
                slotComponent.m_Recipe = id;
                Text text = slotInstance.GetComponent<Text>();
                text.text = item.name;
            }

            HideInventory();
            OnSetActionBarSelection(0);

            m_ContainerView = new ContainerView();
            m_ContainerView.m_InventoryView = this;
            m_ContainerView.m_IsContainerView = true;

            m_InventoryContainerView = new ContainerView();
            m_InventoryContainerView.m_InventoryView = this;
            m_InventoryContainerView.m_IsContainerView = false;

            isInitialized = true;
        }
	}

    public void ShowCraftingMenu()
    {
        m_CraftingPanel.SetActive(true);
        Player player = Player.GetLocalPlayer();
        Assert.IsNotNull(player);
        LocalRecipeStore recipeStore = player.GetComponent<LocalRecipeStore>();
        Assert.IsNotNull(recipeStore);

        for (RecipeId recipe = 0; recipe < RecipeId.MaxRecipes; ++recipe)
        {
            if (recipeStore.IsRecipeKnown(recipe))
            {
                craftingSlots[(int)recipe].SetActive(true);
            }
        }
    }
    public int GetSelectedActionBarSlot()
    {
        return selectedActionBarSlot;
    }

    public void ShowInventory()
    {
        m_InventoryPanel.SetActive(true);
        foreach (GameObject slot in slots)
        {
            Outline outline = slot.GetComponentInChildren<Outline>();
            outline.enabled = false;
        }
        Outline newOutline = slots[inventorySlotCount + selectedActionBarSlot].GetComponentInChildren<Outline>();
        newOutline.enabled = true;
    }

    public void HideInventory()
    {
        m_InventoryPanel.SetActive(false);
        for (int i = 0; i < actionBarSlotCount; ++i)
        {
            Outline outline = slots[inventorySlotCount + i].GetComponentInChildren<Outline>();
            outline.enabled = false;
        }
        Outline newOutline = slots[inventorySlotCount + selectedActionBarSlot].GetComponentInChildren<Outline>();
        newOutline.enabled = true;
    }

    public void ShowContainerSlots()
    {
        m_CraftingPanel.SetActive(false);
        m_ContainerSlots.SetActive(true);
    }

    public void HideContainerSlots()
    {
        m_ContainerSlots.SetActive(false);
        ShowCraftingMenu();
    }

    public void SetItemInContainerSlot(ItemSlot item, int slot)
    {
        Assert.IsTrue(slot < containerSlotCount);
        UpdateSlotObject(containerSlots[slot], item);
    }

    static void UpdateSlotObject(GameObject slotObject, ItemSlot item)
    {
        Text text = slotObject.GetComponentInChildren<Text>();
        if (item.id != ItemID.None)
        {
            ItemDbEntry entry = ItemDatabase.GetInstance().GetItemById(item.id);
            if (item.quantity > 1)
            {
                text.text = entry.name + " (" + item.quantity + ")";
            }
            else
            {
                text.text = entry.name;
            }
        }
        else
        {
            text.text = "";
        }
    }
    public void OnSetItemInSlot(ItemSlot item, int slot)
    {
        Assert.IsTrue(slot < totalSlotCount);
        UpdateSlotObject(slots[slot], item);
    }

    public void OnSetActionBarSelection(int slot)
    {
        Assert.IsTrue(slot < actionBarSlotCount);
        if (selectedActionBarSlot >= 0)
        {
            Outline previousOutline = slots[inventorySlotCount + selectedActionBarSlot].GetComponentInChildren<Outline>();
            previousOutline.enabled = false;
        }
        Outline newOutline = slots[inventorySlotCount + slot].GetComponentInChildren<Outline>();
        newOutline.enabled = true;
        selectedActionBarSlot = slot;
    }
}
