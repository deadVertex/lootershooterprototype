﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Assertions;

public class CraftingSlot : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    public RecipeId m_Recipe;
    private Text m_Text;

    void Start()
    {
        m_Text = GetComponent<Text>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Player.GetLocalPlayer().StartCraft(m_Recipe);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        m_Text.color = Color.white;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        m_Text.color = new Color(219, 219, 219);
    }
}
