﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Slot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    public int index;
    public bool isContainer;
    public static Slot selectedSlot;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
        Outline outline = GetComponentInChildren<Outline>();
        outline.enabled = true;
        selectedSlot = this;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Outline outline = GetComponentInChildren<Outline>();
        outline.enabled = false;
        if (selectedSlot == this)
        {
            selectedSlot = null;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (this != selectedSlot)
        {
            if (selectedSlot != null)
            {
                Player.GetLocalPlayer().OnDragItem(this.index, this.isContainer, selectedSlot.index, selectedSlot.isContainer);
            }
            else
            {
                Player.GetLocalPlayer().OnDragItem(this.index, this.isContainer, -1, false);
            }
        }
    }
}
