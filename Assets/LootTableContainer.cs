﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class LootTableRow
{
    public ArrayList m_ItemSlots = new ArrayList();
    public uint m_Weight;
    public float m_Probability;
    public uint m_LowerBound;
    public uint m_UpperBound;
};

class LootTable
{
    private ArrayList m_Rows = new ArrayList();
    private uint m_TotalWeight;
    public uint m_MaxItemCount;

    public void AddItem(ItemID item, ushort quantity, uint weight)
    {
        LootTableRow row = new LootTableRow();
        ItemSlot itemSlot = new ItemSlot();
        itemSlot.id = item;
        itemSlot.quantity = quantity;
        row.m_ItemSlots.Add(itemSlot);
        row.m_Weight = weight;
        m_Rows.Add(row);
    }

    public void AddItems(ItemID item1, ushort quantity1, ItemID item2, ushort quantity2, uint weight)
    {
        LootTableRow row = new LootTableRow();
        ItemSlot[] itemSlots = new ItemSlot[2];
        itemSlots[0].id = item1;
        itemSlots[0].quantity = quantity1;
        itemSlots[1].id = item2;
        itemSlots[1].quantity = quantity2;
        row.m_ItemSlots.Add(itemSlots[0]);
        row.m_ItemSlots.Add(itemSlots[1]);
        row.m_Weight = weight;
        m_Rows.Add(row);
    }

    public void BalanceTable()
    {
        m_TotalWeight = 0;
        foreach (LootTableRow row in m_Rows)
        {
            row.m_LowerBound = m_TotalWeight;
            m_TotalWeight += row.m_Weight;
            row.m_UpperBound = m_TotalWeight;
        }

        foreach (LootTableRow row in m_Rows)
        {
            if (row.m_ItemSlots.Count > m_MaxItemCount)
            {
                m_MaxItemCount = (uint)row.m_ItemSlots.Count;
            }

            row.m_Probability = (float)row.m_Weight / (float)m_TotalWeight;
        }
    }

    public ArrayList GetEntry()
    {
        int x = Random.Range(0, (int)m_TotalWeight);
        foreach (LootTableRow row in m_Rows)
        {
            if (x >= row.m_LowerBound)
            {
                if (x < row.m_UpperBound)
                {
                    return row.m_ItemSlots;
                }
            }
        }
        return null;
    }

};

public enum LootTableId
{
    CommonWeapons,
    MaxLootTables
};

public class LootTableDatabase {

    LootTable[] m_LootTables = new LootTable[(int)LootTableId.MaxLootTables];

    static private LootTableDatabase instance = null;

	void Initialize () {
        LootTable commonWeaponsLootTable = new LootTable();
        commonWeaponsLootTable.AddItem(ItemID.Machete, 1, 40);
        commonWeaponsLootTable.AddItems(ItemID.Bow, 1, ItemID.StoneArrow, 5, 50);
        commonWeaponsLootTable.AddItems(ItemID.Crossbow, 1, ItemID.StoneArrow, 5, 10);
        commonWeaponsLootTable.BalanceTable();

        m_LootTables[(int)LootTableId.CommonWeapons] = commonWeaponsLootTable;
	}

    public static LootTableDatabase GetInstance()
    {
        if (instance == null)
        {
            instance = new LootTableDatabase();
            instance.Initialize();
        }
        return instance;
    }

    public uint GetMaxItemsInLootTable(LootTableId tableId)
    {
        return m_LootTables[(int)tableId].m_MaxItemCount;
    }

    public ArrayList GetLootFromTable(LootTableId tableId)
    {
        return m_LootTables[(int)tableId].GetEntry();
    }
}

public class LootTableContainer : Container {

    public LootTableId m_LootTableId;

	void Start () {
        var lootTableDatabase = LootTableDatabase.GetInstance();
        var capacity = lootTableDatabase.GetMaxItemsInLootTable(m_LootTableId);
        Initialize((int)capacity, false);		
        if (isServer)
        {
            var list = lootTableDatabase.GetLootFromTable(m_LootTableId);
            foreach (ItemSlot slot in list)
            {
                sv_AddItem(slot);
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
