﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour {

    public delegate void OnFireDelegate();
    public OnFireDelegate m_OnFire;
    public GameObject m_BulletHolePrefab;
    public float m_RateOfFire;
    BulletManager m_BulletManager = new BulletManager();
    Camera m_Camera;
    float m_NextShootTime;
    UnityStandardAssets.Characters.FirstPerson.MouseLook m_MouseLook;

	// Use this for initialization
	void Start () {
        m_Camera = GetComponent<Camera>();
        var controller = GetComponentInParent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>();
        m_MouseLook = controller.m_MouseLook;
	}
	
	void FixedUpdate () {
        m_BulletManager.FixedUpdate(Time.fixedDeltaTime);
        // Process impact events
        for (uint i = 0; i < m_BulletManager.m_ImpactEventCount; ++i)
        {
            var impactEvent = m_BulletManager.m_ImpactEvents[i];
            GameObject bulletHole = Instantiate(m_BulletHolePrefab, impactEvent.position,
                Quaternion.LookRotation(impactEvent.normal));
        }
	}

    void Update()
    {
	    if (Input.GetKey(KeyCode.Mouse0))
        {
            if (Time.time > m_NextShootTime)
            {
                Ray ray = m_Camera.ScreenPointToRay(new Vector2(Screen.width / 2, Screen.height / 2));
                m_BulletManager.SpawnBullet(ray.origin, ray.direction, 20000.0f, Time.fixedDeltaTime);
                m_OnFire();
                float x = Random.Range(0.4f, 1.2f);
                float y = Random.Range(-0.2f, 0.2f);
                m_MouseLook.ApplyViewKick(x, y);

                float timeBetweenShots = 1.0f / (m_RateOfFire / 60.0f);
                m_NextShootTime = Time.time + timeBetweenShots;
            }
        }

        m_BulletManager.Update(Time.deltaTime);
    }
}
