﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Networking;

public class Hunger : NetworkBehaviour
{
    public float maxFood;
    [SyncVar]
    public float food;
    public bool loseHealthWhenStarving;
    public float consumptionRate;
    public float healthLossRate;

    private Killable healthComponent;

    void Start()
    {
        if (!isServer)
        {
            return;
        }
        if (loseHealthWhenStarving)
        {
            healthComponent = GetComponent<Killable>();
            Assert.IsNotNull(healthComponent);
        }
    }
    void Update()
    {
        if (!isServer)
        {
            return;
        }

        if (food > 0.0f)
        {
            food -= consumptionRate * Time.deltaTime;
        }
        else if (loseHealthWhenStarving)
        {
            healthComponent.ReceiveDamage(healthLossRate * Time.deltaTime);
        }
    }

    public void sv_RestoreFood(float amount)
    {
        Assert.IsTrue(isServer);
        food = Mathf.Min(food + amount, maxFood);
    }

}
