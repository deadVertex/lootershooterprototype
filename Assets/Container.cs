﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Assertions;

public struct ItemSlot
{
    public ItemID id;
    public ushort quantity;
};
public interface IContainerObserver
{
    void ClearAllContainerSlots();
    void OnSetItemInSlot(ItemSlot item, int slot);
};

public class Container : NetworkBehaviour {

    private ItemSlot[] items;

    private IContainerObserver m_observer;
    private bool m_CanStoreItems;

    public void Initialize(int capacity, bool canStoreItems)
    {
        items = new ItemSlot[capacity];
        m_CanStoreItems = canStoreItems;
    }

    [Server]
    public void sv_Sync()
    {
        if (!isLocalPlayer)
        {
            RpcOnClear();
            for (int slot = 0; slot < items.Length; ++slot)
            {
                if (items[slot].id != ItemID.None)
                {
                    RpcOnSetItemInSlot(items[slot], slot);
                }
            }
        }
    }

    public void SetObserver(IContainerObserver observer, bool updateObserver = false)
    {
        m_observer = observer;
        if (updateObserver)
        {
            m_observer.ClearAllContainerSlots();
            for (int i = 0; i < items.Length; ++i)
            {
                if (items[i].id != ItemID.None)
                {
                    m_observer.OnSetItemInSlot(items[i], i);
                }
            }
        }
    }

    private void SetItemInSlotInternal(ItemSlot item, int slot)
    {
        Assert.IsTrue(slot < items.Length);
        items[slot] = item;
        if (m_observer != null)
        {
            m_observer.OnSetItemInSlot(item, slot);
        }
    }

    // TODO: How to handle stack size limits
    [Server]
    public bool sv_CombineWithItemInSlot(ItemSlot item, int slot)
    {
        ItemSlot itemInSlot = GetItemInSlot(slot);

        if (item.id == itemInSlot.id)
        {
            ItemDbEntry entry = ItemDatabase.GetInstance().GetItemById(item.id);
            if (entry.isStackable)
            {
                // Combine
                itemInSlot.quantity += item.quantity;
                sv_SetItemInSlot(itemInSlot, slot);
                return true;
            }
        }
        return false;
    }

    [Server]
    public void sv_SetItemInSlot(ItemSlot item, int slot)
    {
        SetItemInSlotInternal(item, slot);
        if (!isLocalPlayer)
        {
            RpcOnSetItemInSlot(item, slot);
        }
    }


    [ClientRpc]
    public void RpcOnSetItemInSlot(ItemSlot item, int slot)
    {
        //Debug.Log("SetItemInSlot " + slot);
        SetItemInSlotInternal(item, slot);
    }

    public bool sv_AddItem(ItemSlot item)
    {
        ItemDbEntry entry = ItemDatabase.GetInstance().GetItemById(item.id);

        // Try stack the item in an existing slot before using up an empty slot
        if (entry.isStackable)
        {
            for (int i = 0; i < items.Length; ++i)
            {
                if (items[i].id == item.id)
                {
                    ItemSlot existingSlot = GetItemInSlot(i);
                    existingSlot.quantity += item.quantity;
                    sv_SetItemInSlot(existingSlot, i);
                    return true;
                }
            }
        }

        for (int i = 0; i < items.Length; ++i)
        {
            if (items[i].id == ItemID.None)
            {
                sv_SetItemInSlot(item, i);
                return true;
            }
        }
        return false;
    }

    public ItemSlot GetItemInSlot(int slot)
    {
        Assert.IsTrue(slot < items.Length);
        return items[slot];
    }

    public int FindItemInSlotById(ItemID id)
    {
        for (int i = 0; i < items.Length; ++i)
        {
            if (items[i].id == id)
            {
                return i;
            }
        }
        return -1;
    }

    public ArrayList GetAsContainerContents()
    {
        ArrayList result = new ArrayList();
        foreach (ItemSlot item in items)
        {
            if (item.id != ItemID.None)
            {
                ItemSlot slot = new ItemSlot();
                slot.id = item.id;
                slot.quantity = 1;
                result.Add(slot);
            }
        }
        return result;
    }

    public void sv_ClearSlot(int slot)
    {
        ItemSlot nullSlot = new ItemSlot();
        sv_SetItemInSlot(nullSlot, slot);
    }

    public void sv_ClearContents()
    {
        ItemSlot nullSlot = new ItemSlot();
        for (int i = 0; i < items.Length; ++i)
        {
            SetItemInSlotInternal(nullSlot, i);
        }

        if (!isLocalPlayer)
        {
            RpcOnClear();
        }
    }

    [ClientRpc]
    private void RpcOnClear()
    {
        ItemSlot nullSlot = new ItemSlot();
        for (int i = 0; i < items.Length; ++i)
        {
            SetItemInSlotInternal(nullSlot, i);
        }
    }

    public bool sv_ConsumeItemById(ItemID id)
    {
        int slot = FindItemInSlotById(id);
        if (slot >= 0)
        {
            return sv_ConsumeSlot(slot);
        }
        return false;
    }

    // NOTE: It is assumed that the inventory contains the amount trying to be consumed
    [Server]
    public void sv_ConsumeItemsWithId(ItemID id, int quantity)
    {
        int amountToConsume = quantity;
        for (int i = 0; i < items.Length; ++i)
        {
            if (items[i].id == id)
            {
                ItemSlot item = items[i];
                if (amountToConsume < item.quantity)
                {
                    // Quantity remaining in slot
                    int newQuantity = (int)item.quantity - amountToConsume;
                    Assert.IsTrue(newQuantity >= 1);
                    item.quantity = (ushort)newQuantity;
                    sv_SetItemInSlot(item, i);
                    amountToConsume = 0;
                    break;
                }
                else
                {
                    // Consume entire slot
                    amountToConsume -= item.quantity;
                    sv_ClearSlot(i);
                    if (amountToConsume == 0)
                    {
                        break;
                    }
                }
            }
        }
        // The caller of this function should have checked that we have enough items 
        // to consume, if this fails then we have removed items from the inventory 
        // but not the required amount, leaving it in a bad state.
        Assert.IsTrue(amountToConsume == 0);
    }

    public bool sv_ConsumeSlot(int slot)
    {
        ItemSlot item = items[slot];
        Assert.AreNotEqual(item.quantity, 0);
        if (item.quantity > 1)
        {
            item.quantity -= 1;
            sv_SetItemInSlot(item, slot);
        }
        else
        {
            sv_ClearSlot(slot);
        }
        return true;
    }


    public int CountItem(ItemID id)
    {
        int total = 0;
        for (int i = 0; i < items.Length; ++i)
        {
            if (items[i].id == id)
            {
                total += items[i].quantity;
            }
        }
        return total;
    }

    public bool CanStoreItems()
    {
        return m_CanStoreItems;
    }
}
