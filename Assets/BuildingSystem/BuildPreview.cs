﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildPreview : MonoBehaviour {

    public Material ValidMaterial;
    public Material InvalidMaterial;

    private List<Collider> m_Colliders = new List<Collider>();
    private bool m_CanBuild;

    void OnTriggerEnter(Collider other)
    {
        m_Colliders.Add(other);
    }

    void OnTriggerExit(Collider other)
    {
        m_Colliders.Remove(other);
    }

    void Update()
    {
        bool wasChanged = false;
        if (m_Colliders.Count == 0)
        {
            wasChanged = m_CanBuild != true;
            m_CanBuild = true;
        }
        else
        {
            wasChanged = m_CanBuild != false;
            m_CanBuild = false;
        }

        if (wasChanged)
        {
            Renderer rendererComponent = GetComponent<Renderer>();
            if (m_CanBuild)
            {
                rendererComponent.material = ValidMaterial;
            }
            else
            {
                rendererComponent.material = InvalidMaterial;
            }
        }
    }

    public bool CanBuild()
    {
        return m_CanBuild;
    }

}
