﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Assertions;

public enum BuildingPartType
{
    Foundation,
    Pillar,
    Wall,
    Ceiling,
    Stairs,
    StorageBox,
}

[System.Serializable]
public struct BuildingPart
{
    public GameObject previewPrefab;
    public GameObject prefab;
    public BuildingPartType type;
};

public interface IBuildingSystemObserver
{
    void sv_OnObjectPlaced(int partIdx);
};

public class BuildingSystem : NetworkBehaviour
{
    public BuildingPart[] parts;

    public Transform cameraTransform;

    private GameObject m_Preview;
    private BuildPreview m_PreviewComponent;
    private int m_ActivePartIdx = -1;

    private IBuildingSystemObserver m_Observer;

    public void SetObserver(IBuildingSystemObserver observer)
    {
        m_Observer = observer;
    }

    [Client]
    public void ShowPreview(int idx)
    {
        Assert.IsTrue(idx < parts.Length);
        BuildingPart part = parts[idx];
        if (idx != m_ActivePartIdx)
        {
            if (m_Preview != null)
            {
                Destroy(m_Preview);
            }
            m_Preview = Instantiate(part.previewPrefab);
            m_PreviewComponent = m_Preview.GetComponent<BuildPreview>();
            m_ActivePartIdx = idx;
        }
    }

    [Client]
    public void HidePreview()
    {
        if (m_Preview != null)
        {
            m_PreviewComponent = null;
            Destroy(m_Preview);
        }
        m_ActivePartIdx = -1;
    }

    static float SnapToGrid(float v, float gridSize)
    {
        int div = (int)(v / gridSize);
        float prev = div * gridSize;
        float next = prev + gridSize * Mathf.Sign(v);
        if (Mathf.Abs(v - prev) < Mathf.Abs(next - v))
        {
            return prev;
        }
        else
        {
            return next;
        }
    }

    static bool SnapToCorner(Vector3 foundationPosition, Vector3 hitPoint, float maxError, out Vector3 snapPoint)
    {
        float size = 1.5f;
        Vector2[] corners = new Vector2[4];
        corners[0].x = -size;
        corners[0].y = size;
        corners[1].x = -size;
        corners[1].y = -size;
        corners[2].x = size;
        corners[2].y = -size;
        corners[3].x = size;
        corners[3].y = size;

        Vector2 delta = new Vector2(hitPoint.x, hitPoint.z) - new Vector2(foundationPosition.x, foundationPosition.z);
        float[] distances = new float[4];
        for (int i = 0; i < corners.Length; ++i)
        {
            distances[i] = Vector2.Distance(corners[i], delta);
        }
        float minDist = maxError;
        int minIdx = -1;
        for (int i = 0; i < distances.Length; ++i)
        {
            if (distances[i] < minDist)
            {
                minDist = distances[i];
                minIdx = i;
            }
        }

        if (minIdx >= 0)
        {
            snapPoint.x = foundationPosition.x + corners[minIdx].x;
            snapPoint.y = hitPoint.y;
            snapPoint.z = foundationPosition.z + corners[minIdx].y;
            return true;
        }
        else
        {
            snapPoint = hitPoint;
            return false;
        }
    }
    static bool SnapToSide(Vector3 foundationPosition, Vector3 hitPoint, float maxError, out Vector3 snapPoint)
    {
        float size = 1.5f;
        Vector2[] corners = new Vector2[4];
        corners[0].x = -size;
        corners[0].y = 0.0f;
        corners[1].x = 0.0f;
        corners[1].y = -size;
        corners[2].x = size;
        corners[2].y = 0.0f;
        corners[3].x = 0.0f;
        corners[3].y = size;

        Vector2 delta = new Vector2(hitPoint.x, hitPoint.z) - new Vector2(foundationPosition.x, foundationPosition.z);
        float[] distances = new float[4];
        for (int i = 0; i < corners.Length; ++i)
        {
            distances[i] = Vector2.Distance(corners[i], delta);
        }
        float minDist = maxError;
        int minIdx = -1;
        for (int i = 0; i < distances.Length; ++i)
        {
            if (distances[i] < minDist)
            {
                minDist = distances[i];
                minIdx = i;
            }
        }

        if (minIdx >= 0)
        {
            snapPoint.x = foundationPosition.x + corners[minIdx].x;
            snapPoint.y = hitPoint.y;
            snapPoint.z = foundationPosition.z + corners[minIdx].y;
            return true;
        }
        else
        {
            snapPoint = hitPoint;
            return false;
        }
    }

    [Client]
    void Update()
    {
        if (m_ActivePartIdx != -1)
        {
            BuildingPart activePart = parts[m_ActivePartIdx];
            RaycastHit hitInfo;
            if (Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hitInfo, 10.0f))
            {
                float gridSize = 1.0f;
                if (activePart.type == BuildingPartType.Foundation)
                {
                    if (hitInfo.collider.tag == "buildable")
                    {
                        Vector3 p = hitInfo.point;
                        Vector3 v = new Vector3(SnapToGrid(p.x, gridSize), p.y, SnapToGrid(p.z, gridSize));
                        m_Preview.transform.position = v;
                    }
                }
                else if (activePart.type == BuildingPartType.Pillar)
                {
                    if (hitInfo.collider.tag == "building")
                    {
                        PlacedBuildingPart placedPart = hitInfo.collider.GetComponent<PlacedBuildingPart>();
                        Assert.IsNotNull(placedPart);
                        if (placedPart.type == BuildingPartType.Foundation)
                        {
                            if (Vector3.Dot(hitInfo.normal, Vector3.up) == 1.0f)
                            {
                                Vector3 p;
                                if (SnapToCorner(hitInfo.collider.transform.position, hitInfo.point, 1.0f, out p))
                                {
                                    p.y += 1.5f;
                                    m_Preview.transform.position = p;
                                }
                            }
                        }
                        else if (placedPart.type == BuildingPartType.Pillar)
                        {
                            Vector3 p = hitInfo.collider.transform.position;
                            p.y += 3.0f;
                            m_Preview.transform.position = p;
                        }
                    }
                }
                else if (activePart.type == BuildingPartType.Wall)
                {
                    if (hitInfo.collider.tag == "building")
                    {
                        PlacedBuildingPart placedPart = hitInfo.collider.GetComponent<PlacedBuildingPart>();
                        Assert.IsNotNull(placedPart);
                        if (placedPart.type == BuildingPartType.Foundation ||
                            placedPart.type == BuildingPartType.Ceiling)
                        {
                            if (Vector3.Dot(hitInfo.normal, Vector3.up) == 1.0f)
                            {
                                Vector3 p;
                                if (SnapToSide(hitInfo.collider.transform.position, hitInfo.point, 1.0f, out p))
                                {
                                    p.y += 1.5f;
                                    m_Preview.transform.position = p;
                                }
                            }
                        }
                        else if (placedPart.type == BuildingPartType.Wall)
                        {
                            Vector3 p = hitInfo.collider.transform.position;
                            p.y += 3.0f;
                            m_Preview.transform.position = p;
                        }
                    }
                }
                else if (activePart.type == BuildingPartType.Ceiling)
                {
                    if (hitInfo.collider.tag == "building")
                    {
                        PlacedBuildingPart placedPart = hitInfo.collider.GetComponent<PlacedBuildingPart>();
                        Assert.IsNotNull(placedPart);
                        //if (placedPart.type == BuildingPartType.Pillar)
                        //{
                        //    if (Vector3.Dot(hitInfo.normal, Vector3.up) == 0.0f)
                        //    {
                        //        Vector3 p;
                        //        if (SnapToSide(hitInfo.collider.transform.position, hitInfo.point, 1.0f, out p))
                        //        {
                        //            p.y += 1.25f;
                        //            m_Preview.transform.position = p;
                        //        }
                        //    }
                        //}
                        if (placedPart.type == BuildingPartType.Wall)
                        {
                            if (Vector3.Dot(hitInfo.normal, Vector3.up) == 0.0f)
                            {
                                Vector3 p = hitInfo.collider.transform.position;
                                p += hitInfo.normal * 1.5f;
                                p.y += 1.5f;
                                m_Preview.transform.position = p;
                            }
                        }
                    }
                }
                else if (activePart.type == BuildingPartType.Stairs)
                {
                    if (hitInfo.collider.tag == "building")
                    {
                        PlacedBuildingPart placedPart = hitInfo.collider.GetComponent<PlacedBuildingPart>();
                        Assert.IsNotNull(placedPart);
                        if (placedPart.type == BuildingPartType.Foundation ||
                            placedPart.type == BuildingPartType.Ceiling)
                        {
                            if (Vector3.Dot(hitInfo.normal, Vector3.up) == 1.0f)
                            {
                                Vector3 p = hitInfo.collider.transform.position;
                                p.y = hitInfo.point.y + 1.5f;
                                m_Preview.transform.position = p;
                            }
                        }
                    }
                }
                else if (activePart.type == BuildingPartType.StorageBox)
                {
                    //if (hitInfo.collider.tag == "building")
                    {
                        //PlacedBuildingPart placedPart = hitInfo.collider.GetComponent<PlacedBuildingPart>();
                        //Assert.IsNotNull(placedPart);
                        //if (placedPart.type == BuildingPartType.Foundation ||
                        //    placedPart.type == BuildingPartType.Ceiling)
                        {
                            if (Vector3.Dot(hitInfo.normal, Vector3.up) == 1.0f)
                            {
                                Vector3 p = hitInfo.point;
                                p.y += 0.35f;
                                m_Preview.transform.position = p;
                            }
                        }
                    }
                }
            }
        }
    }

    [Client]
    public void PlaceObject()
    {
        if (m_ActivePartIdx != -1)
        {
            if (m_PreviewComponent.CanBuild())
            {
                CmdPlaceObject(m_ActivePartIdx, m_Preview.transform.position, m_Preview.transform.rotation);
            }
        }
    }

    [Client]
    public void RotateObject()
    {
        if (IsBuilding())
        {
            if (m_ActivePartIdx != -1)
            {
                BuildingPart activePart = parts[m_ActivePartIdx];
                if (activePart.type == BuildingPartType.Stairs)
                {
                    // Stairs are a mesh, which is on the wrong axis
                    m_PreviewComponent.transform.Rotate(Vector3.forward, 90.0f);
                }
                else
                {
                    m_PreviewComponent.transform.Rotate(Vector3.up, 90.0f);
                }
            }
        }
    }

    [Command]
    public void CmdPlaceObject(int partIdx, Vector3 position, Quaternion rotation)
    {
        BuildingPart activePart = parts[partIdx];
        GameObject part = Instantiate(activePart.prefab, position, rotation);
        NetworkServer.Spawn(part);
        if (m_Observer != null)
        {
            m_Observer.sv_OnObjectPlaced(partIdx);
        }
    }

    public bool IsBuilding()
    {
        return m_ActivePartIdx >= 0;
    }
}
