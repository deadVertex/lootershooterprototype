﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageContainer : Container {

    public int m_NumStorageSlots;

	// Use this for initialization
	void Start ()
    {
        Initialize(m_NumStorageSlots, true);		
	}
}
