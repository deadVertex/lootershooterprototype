﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasingFunctions
{
    public static float QuadInOut(float t)
    {
        return t < 0.5f ? 2.0f * t * t : -1.0f + (4.0f - 2.0f * t) * t;
    }
}
