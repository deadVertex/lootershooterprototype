﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Networking;
using System.Collections;

public class RandomizedContainer : Container {

    public ItemRarity minRarity;
    public ItemRarity maxRarity;
    public int minItemCount;
    public int maxItemCount;

	void Start ()
    {
        Initialize(maxItemCount, false);
        if (isServer)
        {
            Assert.IsTrue(maxRarity != ItemRarity.MaxRarity);

            int itemCount = Random.Range(minItemCount, maxItemCount + 1);
            for (int i = 0; i < itemCount; ++i)
            {
                ItemRarity rarity = (ItemRarity)Random.Range((int)minRarity, (int)maxRarity + 1);
                ItemDbEntry entry = ItemDatabase.GetInstance().GetRandomItemByRarity(rarity);
                ushort quantity = 1;
                if (entry.isStackable)
                {
                    quantity = (ushort)Random.Range(1, 5);
                }
                ItemSlot item = new ItemSlot();
                item.id = entry.id;
                item.quantity = quantity;
                sv_AddItem(item);
            }
        }
	}
}
