﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewModel : MonoBehaviour {

    public Vector3 m_AimPosition;
    public float m_AimFov;
    public float m_AimSpeed;
    public GameObject m_MuzzleFlash;
    public GameObject m_RoundEject;
    public Camera m_Camera;

    private ParticleSystem m_MuzzleFlashParticleSystem;
    private ParticleSystem m_RoundEjectParticleSystem;
    private Vector3 m_IdlePosition;
    private float m_BaseFov;
    private float m_AimAmount = 0.0f;
    private float m_Recoil;


	// Use this for initialization
	void Start () {
        m_IdlePosition = transform.localPosition;
        m_BaseFov = m_Camera.fieldOfView;
        m_MuzzleFlashParticleSystem = m_MuzzleFlash.GetComponent<ParticleSystem>();
        m_RoundEjectParticleSystem = m_RoundEject.GetComponent<ParticleSystem>();
        var weaponController = GetComponentInParent<WeaponController>();
        weaponController.m_OnFire = OnFire;
	}

    public void OnFire()
    {
        m_MuzzleFlashParticleSystem.Play(true);
        m_RoundEjectParticleSystem.Play(true);
        m_Recoil = 0.1f;
        //Quaternion viewKick = Quaternion.AngleAxis(1.0f, Vector3.right);
        //var localRotation = m_Camera.transform.rotation;
        //Debug.Log(localRotation.eulerAngles);
        //m_Camera.transform.rotation = Quaternion.Euler(localRotation.eulerAngles + Vector3.right * 180.0f);
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey(KeyCode.Mouse1))
        {
            m_AimAmount += Time.deltaTime * m_AimSpeed;
        }
        else
        {
            m_AimAmount -= Time.deltaTime * m_AimSpeed;
        }
        m_AimAmount = Mathf.Clamp01(m_AimAmount);
        float t = EasingFunctions.QuadInOut(m_AimAmount);

        Vector3 localPosition = Vector3.Lerp(m_IdlePosition, m_AimPosition, t);
        UpdateRecoil(Time.deltaTime);
        localPosition.z -= m_Recoil;
        transform.localPosition = localPosition;

        m_Recoil -= m_Recoil * Time.deltaTime * 15.0f;

        m_Camera.fieldOfView = Mathf.Lerp(m_BaseFov, m_AimFov, t);

	}

    void UpdateRecoil(float dt)
    {
    }
}
