﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Networking;
using UnityEngine.AI;

public class RagdollController : NetworkBehaviour {

    static int characterLayer = 2;
    static int ragdollLayer = 13;
    static int hitBoxLayer = 12;
	// Use this for initialization
	void Start () {
        Component[] components = transform.GetComponentsInChildren(typeof(Rigidbody));
        foreach (Rigidbody body in components)
        {
            if (body.gameObject.layer == ragdollLayer)
            {
                body.isKinematic = true;
            }
        }

        Component[] colliders = transform.GetComponentsInChildren(typeof(Collider));

        CharacterController characterController = GetComponent<CharacterController>();
        if (characterController != null)
        {
            foreach (Collider col in colliders)
            {
                Physics.IgnoreCollision(characterController, col);
            }
        }

        foreach (Collider col in colliders)
        {
            if (col.gameObject.layer == ragdollLayer)
            {
                col.enabled = false;
            }
        }
	}

    public void sv_Activate()
    {
        NavMeshAgent navMeshAgent = (NavMeshAgent)GetComponent<NavMeshAgent>();
        Assert.IsNotNull(navMeshAgent);
        Vector3 velocity = navMeshAgent.velocity;
        Activate(velocity);

        if (isServer)
        {
            RpcOnActivate(velocity);
        }
    }

    void Activate(Vector3 velocity)
    {
        CapsuleCollider capsuleCollider = GetComponent<CapsuleCollider>();
        Assert.IsNotNull(capsuleCollider);
        capsuleCollider.enabled = false;

        Enemy enemy = GetComponent<Enemy>();
        Assert.IsNotNull(enemy);
        enemy.enabled = false;

        AiMeleeAttack meleeAttack = GetComponent<AiMeleeAttack>();
        Assert.IsNotNull(meleeAttack);
        meleeAttack.enabled = false;

        NetworkTransform netTransform = (NetworkTransform)GetComponent<NetworkTransform>();
        Assert.IsNotNull(netTransform);
        netTransform.enabled = false;

        Component[] components = transform.GetComponentsInChildren(typeof(Rigidbody));
        foreach (Rigidbody body in components)
        {
            if (body.gameObject.layer == ragdollLayer)
            {
                body.isKinematic = false;
                body.maxDepenetrationVelocity = 1.0f;
            }
        }

        Component[] colliders = transform.GetComponentsInChildren(typeof(Collider));
        foreach (Collider col in colliders)
        {
            if (col.gameObject.layer == ragdollLayer)
            {
                col.enabled = true;
            }
        }

        Animator animator = (Animator)transform.GetComponentInChildren(typeof(Animator));
        Assert.IsNotNull(animator);
        animator.enabled = false;

        NetworkAnimator netAnimator = (NetworkAnimator)transform.GetComponentInChildren(typeof(NetworkAnimator));
        Assert.IsNotNull(netAnimator);
        netAnimator.enabled = false;

        NavMeshAgent navMeshAgent = (NavMeshAgent)GetComponent<NavMeshAgent>();
        Assert.IsNotNull(navMeshAgent);
        navMeshAgent.Stop();
    }

    [ClientRpc]
    void RpcOnActivate(Vector3 velocity)
    {
        Activate(velocity);
    }
}
