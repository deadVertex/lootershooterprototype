﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ItemPickup : NetworkBehaviour {

    [SyncVar]
    public ItemID itemId;
    [SyncVar]
    public ushort quantity = 1;
}
