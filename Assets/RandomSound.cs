﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSound : MonoBehaviour {

    public AudioClip[] sounds;
	// Use this for initialization
	void Start () {
        AudioSource source = GetComponent<AudioSource>();
        int idx = Random.Range(0, sounds.Length);
        source.clip = sounds[idx];
        source.Play();
	}
	
}
