﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Networking;

public class Hitbox : NetworkBehaviour {

    public float damageMultiplier = 1.0f;
    public GameObject m_Owner;

    [Server]
    public void sv_ReceiveDamage(float damage)
    {
        Killable healthComponent = GetComponentInParent<Killable>();
        Assert.IsNotNull(healthComponent);
        healthComponent.ReceiveDamage(damage * damageMultiplier);
    }
}
