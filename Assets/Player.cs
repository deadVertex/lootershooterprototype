﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class Player : NetworkBehaviour, IBuildingSystemObserver
{
    public float interactRange;
    public Camera camera;
    public Text hoverText;
    public Text healthText;
    public Text foodText;
    public float unarmedTimeBetweenAttacks;
    public float unarmedDamagePerAttack;
    public float unarmedAttackRange;
    public GameObject arrowPrefab;
    public AudioClip hitMarkerSound;
    public GameObject itemSatchelPrefab;
    public GameObject m_CrossbowViewModel;
    public GameObject m_BowViewModel;
    public GameObject m_PistolViewModel;
    public GameObject m_AkmViewModel;
    public GameObject m_BulletImpactEffect;
    public AudioClip m_PistolAudioClip;


    private float lastAttackTime = 0.0f;
    private Inventory inventoryComponent;
    private InventoryView inventoryView;
    private UnityStandardAssets.Characters.FirstPerson.PlayerController playerController;
    private bool isInventoryOpen = false;
    [SyncVar]
    private float m_drawTime;
    private float m_maxDrawTime = 1.0f;

    private float m_baseFov;

    private BuildingSystem m_BuildingSystem;
    private AudioSource m_AudioSource;
    private Container m_OpenContainer;
    private AudioSource m_ViewModelAudioSource;
    private Vector3 m_PistolViewModelIdlePosition = new Vector3(0.1058f, -0.0968f, 0.1688f);
    private Vector3 m_PistolViewModelAimPosition = new Vector3(0.0f, -0.065f, 0.1449f);
    private float m_AimDownSightsT = 0.0f;

    private static Player localPlayer;

    static int ragdollLayer = 13;

	// Use this for initialization
	void Start () {
        inventoryComponent = GetComponent<Inventory>();
        Assert.IsNotNull(inventoryComponent);
        if (isLocalPlayer)
        {
            hoverText = GameObject.Find("HudCanvas/HoverText").GetComponent<Text>();
            healthText = GameObject.Find("HudCanvas/HealthText").GetComponent<Text>();
            foodText = GameObject.Find("HudCanvas/FoodText").GetComponent<Text>();

            GameObject canvasHandle = GameObject.Find("CanvasHandle");

            GameObject inventoryCanvas = GameObject.Find("InventoryCanvas");
            Assert.IsNotNull(inventoryCanvas);

            inventoryView = inventoryCanvas.GetComponent<InventoryView>();
            Assert.IsNotNull(inventoryView);
            inventoryView.Start();
            inventoryComponent.SetObserver(inventoryView.m_InventoryContainerView);
            inventoryComponent.SetActionBarObserver(inventoryView);

            playerController = GetComponent<UnityStandardAssets.Characters.FirstPerson.PlayerController>();
            Assert.IsNotNull(playerController);

            GameObject human = transform.Find("human").gameObject;
            Assert.IsNotNull(human);
            SkinnedMeshRenderer[] renderers = human.GetComponentsInChildren<SkinnedMeshRenderer>();
            foreach (SkinnedMeshRenderer r in renderers)
            {
                r.enabled = false;
            }

            Assert.IsNotNull(m_CrossbowViewModel);
            Assert.IsNotNull(m_BowViewModel);
            Assert.IsNotNull(m_PistolViewModel);
            Assert.IsNotNull(m_AkmViewModel);

            m_BuildingSystem = GetComponent<BuildingSystem>();
            m_BuildingSystem.cameraTransform = camera.transform;
            if (isServer)
            {
                m_BuildingSystem.SetObserver(this);
            }

            m_AudioSource = GetComponent<AudioSource>();
            m_ViewModelAudioSource = GetComponentInChildren<AudioSource>();
            Assert.IsNotNull(m_ViewModelAudioSource);

            localPlayer = this;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            m_baseFov = camera.fieldOfView;
        }
	}

    [Server]
    bool Attack(Vector3 origin, Vector3 direction, float range, float damage, float timeBetweenAttacks)
    {
        Assert.IsTrue(isServer);
        if (Time.time > lastAttackTime + timeBetweenAttacks)
        {
            RaycastHit hitInfo;

            int layerMask = Physics.DefaultRaycastLayers & ~(1 << ragdollLayer);
            if (Physics.Raycast(origin, direction, out hitInfo, range, layerMask))
            {
                Hitbox hitBox = hitInfo.collider.GetComponent<Hitbox>();
                if (hitBox)
                {
                    if (hitBox.m_Owner != gameObject)
                    {
                        hitBox.sv_ReceiveDamage(damage);
                        sv_OnHit();
                    }
                }
                else
                {
                    ResourceNode resourceNode = hitInfo.collider.GetComponent<ResourceNode>();
                    if (resourceNode != null)
                    {
                        ItemSlot resource = resourceNode.sv_OnHit();
                        inventoryComponent.sv_AddItem(resource);
                    }
                }
                RpcOnBulletImpact(hitInfo.point, hitInfo.normal);
            }

            lastAttackTime = Time.time;
            return true;
        }
        return false;
    }

    [ClientRpc]
    void RpcOnBulletImpact(Vector3 point, Vector3 normal)
    {
        // TODO: Pool effects?
        GameObject impactEffect = Instantiate(m_BulletImpactEffect, point, Quaternion.LookRotation(normal));
        Destroy(impactEffect, 2.0f);
    }

    public static void SpawnArrow(Vector3 origin, Vector3 direction, float speed, GameObject arrowPrefab, GameObject owner)
    {
        Vector3 up = new Vector3(0.0f, 1.0f, 0.0f);
        Vector3 forward = Vector3.Normalize(direction);
        Vector3 right = Vector3.Cross(forward, up);
        up = Vector3.Cross(forward, right);
        Quaternion rotation = Quaternion.LookRotation(forward, up);
        GameObject arrow = (GameObject)Instantiate(arrowPrefab, origin, rotation);
        Arrow arrowComponent = arrow.GetComponent<Arrow>();
        Assert.IsNotNull(arrowComponent);
        arrowComponent.m_acceleration = forward * speed;
        arrowComponent.m_Owner = owner;
        NetworkServer.Spawn(arrow);
        Destroy(arrow, 600.0f);
    }

    [Command]
    void CmdAttack(Vector3 origin, Vector3 direction)
    {
        ItemSlot activeItem = inventoryComponent.GetSelectedItemFromActionBar();
        if (activeItem.id != ItemID.None)
        {
            ItemDbEntry activeItemData = ItemDatabase.GetInstance().GetItemById(activeItem.id);
            if (activeItemData.type == ItemType.MeleeWeapon)
            {
                Attack(origin, direction, interactRange, activeItemData.damage, activeItemData.timeBetweenAttacks);
            }
            else if (activeItemData.type == ItemType.Gun)
            {
                if (inventoryComponent.FindItemInSlotById(ItemID.Ammo_9mm) >= 0)
                {
                    if (Attack(origin, direction, 100.0f, activeItemData.damage, activeItemData.timeBetweenAttacks))
                    {
                        inventoryComponent.sv_ConsumeItemById(ItemID.Ammo_9mm);
                    }
                }
            }
            else if (activeItemData.type == ItemType.FirstAid)
            {
                if (Time.time > lastAttackTime + activeItemData.timeBetweenAttacks)
                {
                    Killable killableComponent = GetComponent<Killable>();
                    killableComponent.RestoreHealth(activeItemData.healthRestored);
                    int slot = inventoryComponent.SelectedActionBarSlot() + InventoryView.inventorySlotCount;
                    inventoryComponent.sv_ConsumeSlot(slot);
                    lastAttackTime = Time.time;
                }
            }
            else if (activeItemData.type == ItemType.Food)
            {
                if (Time.time > lastAttackTime + activeItemData.timeBetweenAttacks)
                {
                    Hunger hunger = GetComponent<Hunger>();
                    hunger.sv_RestoreFood(activeItemData.foodRestored);
                    int slot = inventoryComponent.SelectedActionBarSlot() + InventoryView.inventorySlotCount;
                    inventoryComponent.sv_ConsumeSlot(slot);
                    lastAttackTime = Time.time;
                }
            }
            else if (activeItemData.type == ItemType.Bow)
            {
                // TODO: Min draw time
                if (m_drawTime > 0.0f)
                {
                    if (inventoryComponent.FindItemInSlotById(ItemID.StoneArrow) >= 0)
                    {
                        if (Time.time > lastAttackTime + activeItemData.timeBetweenAttacks)
                        {
                            float normalizedDrawTime = Mathf.Min(m_drawTime / m_maxDrawTime, 1.0f);
                            SpawnArrow(origin, direction, 3000.0f * normalizedDrawTime, arrowPrefab, gameObject);

                            lastAttackTime = Time.time;
                            inventoryComponent.sv_ConsumeItemById(ItemID.StoneArrow);
                            m_drawTime = 0.0f;
                        }
                    }
                }
            }
            else if (activeItemData.type == ItemType.Crossbow)
            {
                if (inventoryComponent.FindItemInSlotById(ItemID.StoneArrow) >= 0)
                {
                    if (Time.time > lastAttackTime + activeItemData.timeBetweenAttacks)
                    {
                        SpawnArrow(origin, direction, 4000.0f, arrowPrefab, gameObject);

                        lastAttackTime = Time.time;
                        inventoryComponent.sv_ConsumeItemById(ItemID.StoneArrow);
                        m_drawTime = 0.0f;
                    }
                }
            }
            else if (activeItemData.type == ItemType.Blueprint)
            {
                LocalRecipeStore recipeStore = GetComponent<LocalRecipeStore>();
                Assert.IsNotNull(recipeStore);
                recipeStore.sv_AddRecipe(activeItemData.recipe);
                int slot = inventoryComponent.SelectedActionBarSlot() + InventoryView.inventorySlotCount;
                inventoryComponent.sv_ConsumeSlot(slot);
                lastAttackTime = Time.time;
            }
        }
        else
        {
            // Unarmed
            Attack(origin, direction, unarmedAttackRange, unarmedDamagePerAttack, unarmedTimeBetweenAttacks);
        }
    }

    [Command]
    void CmdIncreaseDrawTime()
    {
        m_drawTime += Time.deltaTime;
    }

    [Command]
    void CmdResetDrawTime()
    {
        m_drawTime = 0.0f;
    }

    static float EaseInOutQuad(float t)
    {
        return t < 0.5f ? 2 * t * t : -1 + (4 - 2 * t) * t;
    }

    // Update is called once per frame
    void Update() {


        if (isLocalPlayer)
        {
            CharacterController characterController = GetComponent<CharacterController>();
            Assert.IsNotNull(characterController);
            Vector3 n = characterController.velocity.normalized;
            Animator animator = GetComponentInChildren<Animator>();
            Assert.IsNotNull(animator);
            //animator.SetFloat("vertical", n.z);
            //animator.SetFloat("horizontal", n.x);
        }
        else
        {
            return;
        }

        hoverText.text = "";

        ItemSlot activeItem = inventoryComponent.GetSelectedItemFromActionBar();
        if (activeItem.id == ItemID.Crossbow)
        {
            if (!m_CrossbowViewModel.active)
            {
                m_CrossbowViewModel.SetActive(true);
            }
        }
        else
        {
            if (m_CrossbowViewModel.active)
            {
                m_CrossbowViewModel.SetActive(false);
            }
        }
        if (activeItem.id == ItemID.Bow)
        {
            if (!m_BowViewModel.active)
            {
                m_BowViewModel.SetActive(true);
            }
            Animator animator = m_BowViewModel.GetComponent<Animator>();
            Assert.IsNotNull(animator);
            float drawPercentage = m_drawTime / m_maxDrawTime;
            animator.SetFloat("draw_percentage", drawPercentage);
        }
        else
        {
            if (m_BowViewModel.active)
            {
                m_BowViewModel.SetActive(false);
            }
        }
        if (activeItem.id == ItemID.Pistol)
        {
            if (!m_PistolViewModel.active)
            {
                m_PistolViewModel.SetActive(true);
            }
        }
        else
        {
            if (m_PistolViewModel.active)
            {
                m_PistolViewModel.SetActive(false);
            }
        }
        if (activeItem.id == ItemID.Akm)
        {
            if (!m_AkmViewModel.active)
            {
                m_AkmViewModel.SetActive(true);
            }
        }
        else
        {
            if (m_AkmViewModel.active)
            {
                m_AkmViewModel.SetActive(false);
            }
        }

        if (!isInventoryOpen)
        {
            RaycastHit hitInfo;
            Ray ray = camera.ScreenPointToRay(new Vector2(Screen.width / 2, Screen.height / 2));
            if (activeItem.id != ItemID.None)
            {
                ItemDbEntry activeItemData = ItemDatabase.GetInstance().GetItemById(activeItem.id);
                if (activeItemData.type != ItemType.BuildingPart)
                {
                    if (m_BuildingSystem.IsBuilding())
                    {
                        m_BuildingSystem.HidePreview();
                    }
                }

                if (activeItemData.type == ItemType.BuildingPart)
                {
                    m_BuildingSystem.ShowPreview(activeItemData.buildingPartIdx);
                    Assert.IsTrue(m_BuildingSystem.IsBuilding());
                    if (Input.GetKeyDown(KeyCode.Mouse0))
                    {
                        m_BuildingSystem.PlaceObject();
                    }
                    if (Input.GetKeyDown(KeyCode.Mouse1))
                    {
                        m_BuildingSystem.RotateObject();
                    }
                }
                else if (activeItemData.type == ItemType.Bow)
                {
                    if (Input.GetKeyDown(KeyCode.Mouse0))
                    {
                        if (!m_BowViewModel.active)
                        {
                            m_BowViewModel.SetActive(true);
                        }
                        Animator animator = m_BowViewModel.GetComponent<Animator>();
                        Assert.IsNotNull(animator);
                        animator.SetTrigger("fire");
                        CmdAttack(ray.origin, ray.direction);
                    }
                    if (Input.GetKey(KeyCode.Mouse1))
                    {
                        CmdIncreaseDrawTime();
                    }
                    else
                    {
                        // TODO: Make this play nice with swapping items, because currently if you swap 
                        // items your draw time will not be reset until you swap back to a bow.
                        CmdResetDrawTime();
                    }
                }
                else if (activeItemData.type == ItemType.Gun)
                {
                    if (activeItemData.id == ItemID.Pistol)
                    {
                        float adsTime = 0.1f;
                        float dtFactor = 1.0f / adsTime;
                        if (Input.GetKey(KeyCode.Mouse1))
                        {
                            m_AimDownSightsT = Mathf.Clamp01(m_AimDownSightsT + dtFactor * Time.deltaTime);
                        }
                        else
                        {
                            m_AimDownSightsT = Mathf.Clamp01(m_AimDownSightsT - dtFactor * Time.deltaTime);
                        }
                        float lerpT = EaseInOutQuad(m_AimDownSightsT);
                        Vector3 pistolViewModelPosition = Vector3.Slerp(m_PistolViewModelIdlePosition, m_PistolViewModelAimPosition, lerpT);
                        m_PistolViewModel.transform.localPosition = pistolViewModelPosition;
                    }

                    if (Input.GetKeyDown(KeyCode.Mouse0))
                    {
                        if (inventoryComponent.FindItemInSlotById(ItemID.Ammo_9mm) >= 0)
                        {
                            m_ViewModelAudioSource.clip = m_PistolAudioClip;
                            m_ViewModelAudioSource.Play();
                            if (activeItemData.id == ItemID.Akm)
                            {
                                ViewModel vm = m_AkmViewModel.GetComponent<ViewModel>();
                                vm.OnFire();
                            }
                            CmdAttack(ray.origin, ray.direction);
                        }
                    }
                }
                else if (activeItemData.type == ItemType.MeleeWeapon ||
                         activeItemData.type == ItemType.Bow ||
                         activeItemData.type == ItemType.FirstAid ||
                         activeItemData.type == ItemType.Food ||
                         activeItemData.type == ItemType.Crossbow ||
                         activeItemData.type == ItemType.Blueprint)
                {
                    if (Input.GetKeyDown(KeyCode.Mouse0))
                    {
                        CmdAttack(ray.origin, ray.direction);
                    }
                }
            }
            else
            {
                // Unarmed attack
                // TODO: Simplify logic by introducing a null item
                if (Input.GetKey(KeyCode.Mouse0))
                {
                    CmdAttack(ray.origin, ray.direction);
                }
                if (m_BuildingSystem.IsBuilding())
                {
                    m_BuildingSystem.HidePreview();
                }
            }

            if (Physics.Raycast(ray, out hitInfo, interactRange))
            {
                Killable killable = hitInfo.collider.GetComponent<Killable>();
                if (killable != null)
                {
                    hoverText.text = "Health Remaining: " + killable.GetCurrentHealth();
                }
                else
                {
                    RandomizedContainer randomizedContainer = hitInfo.collider.GetComponent<RandomizedContainer>();
                    if (randomizedContainer != null)
                    {
                        //UpdateHoverText(randomizedContainer.contents);
                    }
                    else
                    {
                        ItemPickup itemPickup = hitInfo.collider.GetComponent<ItemPickup>();
                        if (itemPickup != null)
                        {
                            ItemDbEntry item = ItemDatabase.GetInstance().GetItemById(itemPickup.itemId);
                            Assert.IsNotNull(item);
                            hoverText.text = item.name;
                            if (itemPickup.quantity > 1)
                            {
                                hoverText.text += " (" + itemPickup.quantity + ")";
                            }
                        }
                        else
                        {
                            //Container container = hitInfo.collider.GetComponent<Container>();
                            //if (container != null)
                            //{
                            //    UpdateHoverText(container.contents);
                            //}
                        }
                    }
                }
            }
            if (Input.GetKeyDown(KeyCode.F))
            {
                CmdInteract(ray.origin, ray.direction);
            }
        }
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            m_OpenContainer = null;
            CmdClearOpenContainer();
            if (isInventoryOpen)
            {
                CloseInventory();
            }
            else
            {
                OpenInventory(false);
            }
        }

        int newSelectedActionBarSlot = inventoryComponent.SelectedActionBarSlot();
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            newSelectedActionBarSlot = 0;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            newSelectedActionBarSlot = 1;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            newSelectedActionBarSlot = 2;
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            newSelectedActionBarSlot = 3;
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            newSelectedActionBarSlot = 4;
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            newSelectedActionBarSlot = 5;
        }
        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            newSelectedActionBarSlot = 6;
        }
        if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            newSelectedActionBarSlot = 7;
        }
        if (Input.mouseScrollDelta.y < 0)
        {
            newSelectedActionBarSlot = (newSelectedActionBarSlot + 1) % InventoryView.actionBarSlotCount;
        }
        else if (Input.mouseScrollDelta.y > 0)
        {
            if (newSelectedActionBarSlot > 0)
            {
                newSelectedActionBarSlot = (newSelectedActionBarSlot - 1) % InventoryView.actionBarSlotCount;
            }
            else
            {
                newSelectedActionBarSlot = InventoryView.actionBarSlotCount - 1;
            }
        }

        if (newSelectedActionBarSlot != inventoryComponent.SelectedActionBarSlot())
        {
            CmdSetSelectedActionBarSlot(newSelectedActionBarSlot);
        }

        Killable playerKillable = GetComponent<Killable>();
        healthText.text = "Health: " + Mathf.RoundToInt(playerKillable.GetCurrentHealth());

        Hunger playerHunger = GetComponent<Hunger>();
        foodText.text = "Food: " + Mathf.RoundToInt(playerHunger.food);

        float maxDrawTime = 1.0f;
        float t = Mathf.Min(m_drawTime / maxDrawTime, 1.0f);
        float fovDelta = 5.0f * t;
        //camera.fieldOfView = m_baseFov - fovDelta;
	}

    private void CmdClearOpenContainer()
    {
        m_OpenContainer = null;
    }

    private void OpenInventory(bool showContainerInventory)
    {
        inventoryView.ShowInventory();
        if (showContainerInventory)
        {
            inventoryView.ShowContainerSlots();
        }
        else
        {
            inventoryView.HideContainerSlots();
        }
        playerController.enableMouseLook = false;
        isInventoryOpen = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    private void CloseInventory()
    {
        inventoryView.HideInventory();
        playerController.enableMouseLook = true;
        isInventoryOpen = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void UpdateHoverText(ArrayList contents)
    {
        if (contents.Count > 0)
        {
            string contentsString = "";
            ItemDatabase itemDB = ItemDatabase.GetInstance();
            foreach (ItemSlot slot in contents)
            {
                ItemDbEntry item = itemDB.GetItemById(slot.id);
                if (slot.quantity > 1)
                {
                    contentsString += item.name + " (" + slot.quantity + ")\n";
                }
                else
                {
                    contentsString += item.name + "\n";
                }
            }
            hoverText.text = contentsString;
        }
        else
        {
            hoverText.text = "Empty";
        }
    }

    [Command]
    public void CmdSetSelectedActionBarSlot(int slot)
    {
        inventoryComponent.sv_SetSelectedActionBarSlot(slot);
    }

    // TODO: Move to inventory component?
    private bool sv_TransferItemFromContainerToInventory(ArrayList contents)
    {
        if (contents.Count > 0)
        {
            ItemSlot item = (ItemSlot)contents[0];
            if (inventoryComponent.sv_AddItem(item))
            {
                contents.RemoveAt(0);
                return true;
            }
        }
        return false;
    }

    [Command]
    void CmdInteract(Vector3 origin, Vector3 direction)
    {
        RaycastHit hitInfo;
        Ray ray = new Ray(origin, direction);
        if (Physics.Raycast(ray, out hitInfo, interactRange))
        {
            //RandomizedContainer randomizedContainer = hitInfo.collider.GetComponent<RandomizedContainer>();
            //if (randomizedContainer != null)
            //{
            //    RpcOpenContainer(hitInfo.collider.gameObject);
            //    //if (sv_TransferItemFromContainerToInventory(randomizedContainer.contents))
            //    //{
            //    //    randomizedContainer.RemoveItemAt(0);
            //    //}
            //}
            //else
            //{
                ItemPickup itemPickup = hitInfo.collider.GetComponent<ItemPickup>();
                if (itemPickup != null)
                {
                    ItemSlot item = new ItemSlot();
                    item.id = itemPickup.itemId;
                    item.quantity = itemPickup.quantity;
                    if (inventoryComponent.sv_AddItem(item))
                    {
                        Destroy(itemPickup.gameObject);
                    }
                }
                else
                {
                    Container container = hitInfo.collider.GetComponent<Container>();
                    if (container != null)
                    {
                        m_OpenContainer = container;
                        m_OpenContainer.sv_Sync();
                        RpcOpenContainer(hitInfo.collider.gameObject);
                        //if (sv_TransferItemFromContainerToInventory(container.contents))
                        //{
                        //    container.RemoveItemAt(0);
                        //}
                    }
                }
            //}
        }
    }
    [ClientRpc]
    void RpcOpenContainer(GameObject containerObject)
    {
        if (isLocalPlayer)
        {
            Container container = containerObject.GetComponent<Container>();
            if (container)
            {
                m_OpenContainer = container;
                m_OpenContainer.SetObserver(inventoryView.m_ContainerView, true);
                OpenInventory(true);
            }
            else
            {
                Debug.Log("Game object " + containerObject + " does not have a container component.");
            }
        }
    }

    [Command]
    void CmdMoveItem(int start, bool startIsContainer, int end, bool endIsContainer)
    {
        // Fail move if container is no longer open
        if (startIsContainer || endIsContainer)
        {
            if (m_OpenContainer == null)
            {
                return;
            }
        }

        Container startContainer = null;
        Container endContainer = null;
        if (startIsContainer)
        {
            startContainer = m_OpenContainer;
        }
        else
        {
            startContainer = inventoryComponent;
        }

        if (endIsContainer)
        {
            endContainer = m_OpenContainer;
        }
        else
        {
            endContainer = inventoryComponent;
        }

        ItemSlot startItem = startContainer.GetItemInSlot(start);
        if (startItem.id != ItemID.None)
        {
            if (endContainer.CanStoreItems())
            {
                if (end >= 0)
                {
                    // TODO: Handle stack size limits
                    if (endContainer.sv_CombineWithItemInSlot(startItem, end))
                    {
                        startContainer.sv_ClearSlot(start);
                        // Items stacked
                    }
                    else
                    {
                        ItemSlot endItem = endContainer.GetItemInSlot(end);
                        if (endItem.id == ItemID.None || startContainer.CanStoreItems())
                        {
                            endContainer.sv_SetItemInSlot(startItem, end);
                            startContainer.sv_SetItemInSlot(endItem, start);
                        }
                    }
                }
                else
                {
                    sv_DropItem(startItem);
                    startContainer.sv_ClearSlot(start);
                }
            }
        }
    }

    public void OnDragItem(int start, bool startIsContainer, int end, bool endIsContainer)
    {
        CmdMoveItem(start, startIsContainer, end, endIsContainer);
    }

    public static Player GetLocalPlayer()
    {
        return localPlayer;
    }

    [Command]
    void CmdStartCraft(RecipeId recipeId)
    {
        CraftingRecipe recipe = RecipeDatabase.GetInstance().GetRecipeById(recipeId);
        bool haveAllIngredients = true;
        foreach (ItemSlot ingredient in recipe.ingredients)
        {
            int count = inventoryComponent.CountItem(ingredient.id);
            if (count < ingredient.quantity)
            {
                haveAllIngredients = false;
                break;
            }
        }
        if (haveAllIngredients)
        {
            foreach (ItemSlot ingredient in recipe.ingredients)
            {
                inventoryComponent.sv_ConsumeItemsWithId(ingredient.id, ingredient.quantity);
            }
            // TODO: Craft time
            ItemSlot craftedItem = new ItemSlot();
            craftedItem.id = recipe.result;
            craftedItem.quantity = (ushort)recipe.quantity;
            // TODO: Drop crafted item on ground if inventory is full
            inventoryComponent.sv_AddItem(craftedItem);
        }
        else
        {
            Debug.Log("You do not have all the resources required!");
        }
    }

    public void StartCraft(RecipeId recipe)
    {
        // TODO: Clientside check for ingredients first
        CmdStartCraft(recipe);
    }

    [Server]
    public void sv_OnHit()
    {
        RpcOnHit();
    }

    [ClientRpc]
    void RpcOnHit()
    {
        if (isLocalPlayer)
        {
            m_AudioSource.PlayOneShot(hitMarkerSound);
        }
    }

    [Server]
    public void sv_DropItem(ItemSlot item)
    {
        GameObject itemSatchel = (GameObject)Instantiate(itemSatchelPrefab,
            transform.position + transform.forward, Quaternion.identity);
        itemSatchel.GetComponent<ItemPickup>().itemId = item.id;
        itemSatchel.GetComponent<ItemPickup>().quantity = item.quantity;
        NetworkServer.Spawn(itemSatchel);
    }

    [Server]
    public void sv_OnObjectPlaced(int partIdx)
    {
        ItemSlot activeSlot = inventoryComponent.GetSelectedItemFromActionBar();
        Assert.IsTrue(activeSlot.id != ItemID.None);
        ItemDbEntry activeItemData = ItemDatabase.GetInstance().GetItemById(activeSlot.id);
        Assert.IsTrue(activeItemData.type == ItemType.BuildingPart);
        Assert.IsTrue(activeItemData.buildingPartIdx == partIdx);
        inventoryComponent.sv_ConsumeActiveActionBarSlot();
    }
}
