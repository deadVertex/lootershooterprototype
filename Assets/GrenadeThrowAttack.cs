﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GrenadeThrowAttack : NetworkBehaviour {

    public GameObject grenadePrefab;
    private GameObject m_target;
    private bool targetInRange;
    private float lastThrowTime;
	// Use this for initialization
	void Start () {
		
	}

    // Firing solution from AI for games book
    static bool CalculateFiringSolution(Vector3 start, Vector3 end, float velocity, Vector3 gravity, out Vector3 result)
    {
        Vector3 delta = end - start;
        float a = Vector3.Dot(gravity, gravity);
        float b = -4 * (Vector3.Dot(Physics.gravity, delta) + velocity * velocity);
        float c = 4 * Vector3.Dot(delta, delta);

        if ((4 * a * c) > b * b)
        {
            Debug.Log("No solution available");
            result = Vector3.zero;
            return false;
        }

        float t0 = Mathf.Sqrt((-b + Mathf.Sqrt(b * b - 4 * a * c)) / (2 * a));
        float t1 = Mathf.Sqrt((-b - Mathf.Sqrt(b * b - 4 * a * c)) / (2 * a));
        float t = 0.0f;
        if (t0 < 0.0f)
        {
            if (t1 < 0.0f)
            {
                Debug.Log("No valid times");
                result = Vector3.zero;
                return false;
            }
            else
            {
                t = t1;
            }
        }
        else
        {
            if (t1 < 0.0f)
            {
                t = t0;
            }
            else
            {
                t = Mathf.Min(t0, t1);
            }
        }

        result = ((2 * delta - gravity * t * t) / (2 * velocity * t)) * velocity;
        return true;
    }
	
	// Update is called once per frame
	void Update () {

        if (!isServer)
        {
            return;
        }
        if (Time.time - lastThrowTime > 3.0f)
        {
            if (targetInRange)
            {

                Vector3 targetPosition = m_target.transform.position - new Vector3(0.0f, 0.9f, 0.0f);
                Vector3 direction = Vector3.Normalize(m_target.transform.position - transform.position);

                CharacterController characterController = m_target.GetComponent<CharacterController>();
                Vector3 velocity = new Vector3(characterController.velocity.x, 0.0f, characterController.velocity.z);
                targetPosition += velocity * 1.0f; // Stupid prediction
                targetPosition -= direction * 5.0f; // Aim 5m in front of target
                Vector3 firingVector = Vector3.zero;
                bool found = true;
                if (!CalculateFiringSolution(transform.position, targetPosition, 20.0f, Physics.gravity, out firingVector))
                {
                    if (!CalculateFiringSolution(transform.position, targetPosition, 10.0f, Physics.gravity, out firingVector))
                    {
                        if (!CalculateFiringSolution(transform.position, targetPosition, 5.0f, Physics.gravity, out firingVector))
                        {
                            found = false;
                        }
                    }
                }
                if (found)
                {
                    GameObject grenade = (GameObject)Instantiate(grenadePrefab, transform.position, Quaternion.identity);
                    Rigidbody rigidBody = grenade.GetComponent<Rigidbody>();
                    rigidBody.velocity = firingVector;
                    Grenade grenadeComponent = grenade.GetComponent<Grenade>();
                    grenadeComponent.timeRemaining = Random.RandomRange(0.5f, 3.5f);
                    NetworkServer.Spawn(grenade);
                }
                lastThrowTime = Time.time;
            }
        }
	}

    void OnTriggerEnter(Collider target)
    {
        if (target.tag == "player")
        {
            targetInRange = true;
            m_target = target.gameObject;
        }
    }

    void OnTriggerExit(Collider target)
    {
        if (target.tag == "player")
        {
            targetInRange = false;
        }
    }
}
