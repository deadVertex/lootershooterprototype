﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Assertions;


public class Arrow : NetworkBehaviour {

    public bool m_isActive = true;
    public Vector3 m_acceleration;
    public Vector3 m_velocity;
    public GameObject m_Owner;

    static int arrowLayer = 11;
    static int ragdollLayer = 13;

    void FixedUpdate()
    {
        if (!isServer)
        {
            return;
        }

        if (m_isActive)
        {
            Vector3 startPosition = transform.position;
            m_velocity = CalculateVelocity(m_acceleration, m_velocity, Time.fixedDeltaTime);
            m_acceleration = Vector3.zero;
            Vector3 direction = Vector3.Normalize(m_velocity);
            Quaternion rotation = CalculateRotation(new Vector3(0.0f, 1.0f, 0.0f), direction);
            transform.rotation = rotation;

            Vector3 delta = m_velocity * Time.fixedDeltaTime;
            Vector3 endPosition = startPosition + delta;
            float distance = delta.magnitude;

            Ray ray = new Ray(startPosition, direction);
            RaycastHit hitInfo = new RaycastHit();
            int layerMask = Physics.DefaultRaycastLayers & ~(1 << ragdollLayer) & ~(1 << arrowLayer);
            if (Physics.Raycast(ray, out hitInfo, distance, layerMask))
            {
                if (hitInfo.collider.gameObject != m_Owner)
                {
                    bool disableArrow = false;
                    Hitbox hitBox = hitInfo.collider.GetComponent<Hitbox>();
                    if (hitBox)
                    {
                        if (hitBox.m_Owner != m_Owner)
                        {
                            disableArrow = true;
                            hitBox.sv_ReceiveDamage(70.0f);
                            Player player = m_Owner.GetComponent<Player>();
                            if (player != null)
                            {
                                player.sv_OnHit();
                            }
                        }
                    }
                    else
                    {
                        disableArrow = true;
                    }

                    if (disableArrow)
                    {
                        transform.position = hitInfo.point;
                        transform.SetParent(hitInfo.collider.transform);
                        m_isActive = false;
                    }
                    //// TODO: Remove
                    //Killable healthComponent = hitInfo.collider.GetComponent<Killable>();
                    //if (healthComponent)
                    //{
                    //    healthComponent.ReceiveDamage(70.0f);
                    //    Player player = m_Owner.GetComponent<Player>();
                    //    if (player != null)
                    //    {
                    //        player.sv_OnHit();
                    //    }
                    //}
                }
                else
                {
                    transform.position = endPosition;
                }
            }
            else
            {
                transform.position = endPosition;
            }
        }
    }

    static Vector3 CalculateVelocity(Vector3 acceleration, Vector3 velocity, float timestep)
    {
        acceleration += Physics.gravity;
        float friction = 0.05f;
        acceleration += (velocity * friction);
        Vector3 result = acceleration * timestep + velocity;
        return result;
    }

    static Quaternion CalculateRotation(Vector3 up, Vector3 forward)
    {
        Vector3 right = Vector3.Cross(forward, up);
        up = Vector3.Cross(forward, right);
        Quaternion rotation = Quaternion.LookRotation(forward, up);
        return rotation;
    }
}
