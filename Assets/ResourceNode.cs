﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Assertions;

public class ResourceNode : NetworkBehaviour {

    public ItemID resource;
    public int quantityPerHit = 0;
    public int hitsRemaining = 3;

    [Server]
    public ItemSlot sv_OnHit()
    {
        Assert.IsTrue(hitsRemaining > 0);

        hitsRemaining -= 1;
        ItemSlot result = new ItemSlot();
        result.id = resource;
        result.quantity = (ushort)quantityPerHit;

        if (hitsRemaining == 0)
        {
            Destroy(gameObject);
        }

        return result;
    }

}
