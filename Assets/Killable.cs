﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;

public class Killable : NetworkBehaviour {

    public float baseHealth;
    public bool isPlayer;
    public Vector3 spawnPosition;
    public GameObject backpackPrefab;
    public GameObject ragdollPrefab;
    public GameObject lootTableDropPrefab;
    public LootTableId lootTable;

    public delegate void OnDeathDelegate();

    public OnDeathDelegate m_OnDeath;

    [SyncVar]
    private float health;
    private bool dead = false;

    private GameObject deathCanvas;

    public void Start()
    {
        health = baseHealth;
        if (isLocalPlayer)
        {
            GameObject canvasHandle = GameObject.Find("CanvasHandle");
            deathCanvas = canvasHandle.transform.Find("DeathCanvas").gameObject;

            GameObject respawnButtonObject = deathCanvas.transform.Find("RespawnButton").gameObject;
            Button respawnButton = respawnButtonObject.GetComponent<Button>();

            respawnButton.onClick.AddListener((UnityEngine.Events.UnityAction)this.OnRespawnButtonClick);
        }
    }

    [Server]
    public void ReceiveDamage(float amount)
    {
        if (!isServer)
            return;

        health -= amount;
        if (health <= 0 && !dead)
        {
            if (isPlayer)
            {
                Inventory inventory = GetComponent<Inventory>();
                GameObject backpack = (GameObject)Instantiate(backpackPrefab,
                    transform.position, Quaternion.identity);
                Container container = backpack.GetComponent<Container>();
                ArrayList inventoryContents = inventory.GetAsContainerContents();
                container.Initialize(inventoryContents.Count, false);
                foreach (ItemSlot slot in inventoryContents)
                {
                    container.sv_AddItem(slot);
                }
                NetworkServer.Spawn(backpack);
                inventory.sv_ClearContents();
                gameObject.SetActive(false);
                RpcOnPlayerDeath();
            }
            else
            {
                if (lootTableDropPrefab)
                {
                    GameObject backpack = (GameObject)Instantiate(lootTableDropPrefab,
                        transform.position, Quaternion.identity);
                    LootTableContainer container = backpack.GetComponent<LootTableContainer>();
                    container.m_LootTableId = lootTable;
                    NetworkServer.Spawn(backpack);
                }

                RagdollController ragdollController = GetComponent<RagdollController>();
                if (ragdollController != null)
                {
                    ragdollController.sv_Activate();
                }
                Destroy(gameObject, 600.0f);
            }
            if (m_OnDeath != null)
            {
                m_OnDeath();
            }
            dead = true;
        }
    }

    [ClientRpc]
    void RpcOnPlayerDeath()
    {
        if (isLocalPlayer)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            deathCanvas.SetActive(true);
        }
        gameObject.SetActive(false);
    }

    public void OnRespawnButtonClick()
    {
        CmdRespawnPlayer();
    }

    [Command]
    public void CmdRespawnPlayer()
    {
        Assert.IsTrue(isServer);
        health = baseHealth;
        dead = false;
        RpcOnPlayerRespawn();
    }

    [ClientRpc]
    public void RpcOnPlayerRespawn()
    {
        if (isLocalPlayer)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            deathCanvas.SetActive(false);
        }
        gameObject.SetActive(true);
        gameObject.transform.position = spawnPosition;
    }

    public float GetCurrentHealth()
    {
        return health;
    }

    public void RestoreHealth(float amount)
    {
        if (!isServer)
            return;

        float newHealth = health + amount;
        if (newHealth > baseHealth)
        {
            health = baseHealth;
        }
        else
        {
            health = newHealth;
        }
    }
}
