﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class HitboxController : NetworkBehaviour {

	// Use this for initialization
	void Start () {
        if (isServer)
        {
            Component[] hitboxes = GetComponentsInChildren(typeof(Hitbox));
            foreach (Hitbox hitbox in hitboxes)
            {
                hitbox.m_Owner = gameObject;
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
