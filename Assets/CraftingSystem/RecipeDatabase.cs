﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Assertions;

public enum RecipeId
{
    WoodenClub,
    StoneArrow,
    Crossbow,
    WoodFoundation,
    WoodWall,
    WoodCeiling,
    WoodStairs,
    WoodStorageBox,


    MaxRecipes
};

public struct CraftingRecipe
{
    public RecipeId id;
    public ItemID result;
    public int quantity;
    public ItemSlot[] ingredients;
};

public class RecipeDatabase
{
    private CraftingRecipe[] m_Recipes;
    static private RecipeDatabase m_Instance;

    void Initialize()
    {
        m_Recipes = new CraftingRecipe[(int)RecipeId.MaxRecipes];

        ItemSlot[] ingredients = new ItemSlot[1];
        ingredients[0].id = ItemID.RottingWood;
        ingredients[0].quantity = 100;
        RegisterRecipe(RecipeId.WoodenClub, ItemID.WoodenClub, 1, ingredients);

        ItemSlot[] stoneArrowIngredients = new ItemSlot[1];
        stoneArrowIngredients[0].id = ItemID.RottingWood;
        stoneArrowIngredients[0].quantity = 10;
        RegisterRecipe(RecipeId.StoneArrow, ItemID.StoneArrow, 1, stoneArrowIngredients);

        ItemSlot[] crossbowIngredients = new ItemSlot[1];
        crossbowIngredients[0].id = ItemID.RottingWood;
        crossbowIngredients[0].quantity = 100;
        RegisterRecipe(RecipeId.Crossbow, ItemID.Crossbow, 1, crossbowIngredients);

        ItemSlot[] woodenFoundationIngredients = new ItemSlot[1];
        woodenFoundationIngredients[0].id = ItemID.RottingWood;
        woodenFoundationIngredients[0].quantity = 100;
        RegisterRecipe(RecipeId.WoodFoundation, ItemID.WoodFoundation, 1, woodenFoundationIngredients);

        ItemSlot[] woodenWallIngredients = new ItemSlot[1];
        woodenWallIngredients[0].id = ItemID.RottingWood;
        woodenWallIngredients[0].quantity = 80;
        RegisterRecipe(RecipeId.WoodWall, ItemID.WoodWall, 1, woodenWallIngredients);

        ItemSlot[] woodenCeilingIngredients = new ItemSlot[1];
        woodenCeilingIngredients[0].id = ItemID.RottingWood;
        woodenCeilingIngredients[0].quantity = 60;
        RegisterRecipe(RecipeId.WoodCeiling, ItemID.WoodCeiling, 1, woodenCeilingIngredients);

        ItemSlot[] woodenStairsIngredients = new ItemSlot[1];
        woodenStairsIngredients[0].id = ItemID.RottingWood;
        woodenStairsIngredients[0].quantity = 50;
        RegisterRecipe(RecipeId.WoodStairs, ItemID.WoodStairs, 1, woodenStairsIngredients);

        ItemSlot[] woodenStorageBoxIngredients = new ItemSlot[1];
        woodenStorageBoxIngredients[0].id = ItemID.RottingWood;
        woodenStorageBoxIngredients[0].quantity = 50;
        RegisterRecipe(RecipeId.WoodStorageBox, ItemID.WoodStorageBox, 1, woodenStorageBoxIngredients);
    }

    public static RecipeDatabase GetInstance()
    {
        if (m_Instance == null)
        {
            m_Instance = new RecipeDatabase();
            m_Instance.Initialize();
        }
        return m_Instance;
    }

    private void RegisterRecipe(RecipeId id, ItemID result, int quantity, ItemSlot[] ingredients)
    {
        CraftingRecipe recipe;
        recipe.id = id;
        recipe.result = result;
        recipe.quantity = quantity;
        recipe.ingredients = ingredients;
        m_Recipes[(int)id] = recipe;
    }

    public CraftingRecipe GetRecipeById(RecipeId id)
    {
        CraftingRecipe recipe = m_Recipes[(int)id];
        return recipe;
    }
};