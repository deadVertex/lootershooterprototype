﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;

public class AiMeleeAttack : MonoBehaviour {

    public float attackDelay;
    public float timeBetweenAttacks;
    public float damagePerAttack;
    private float timeUntilNextAttack;
    private float delayRemaining;
    private bool targetInRange;

    static int hitboxLayer = 12;

	// Use this for initialization
	void Start () {
        delayRemaining = attackDelay;	
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        // TODO: Poll rate
        if (!targetInRange)
        {
            // Check for targets in range of our attack
            RaycastHit[] hits = Physics.BoxCastAll(transform.position, new Vector3(0.5f, 0.5f, 0.5f), new Vector3(0.0f, 0.0f, 1.0f), transform.rotation, 1.0f, Physics.IgnoreRaycastLayer);
            foreach (RaycastHit hit in hits)
            {
                if (hit.collider.tag == "player")
                {
                    targetInRange = true;
                    delayRemaining = attackDelay;
                }
            }
        }

        if (targetInRange)
        {
            if (delayRemaining <= 0.0f)
            {
                if (timeUntilNextAttack <= 0.0f)
                {
                    // Perform the actual attack
                    int mask = (1 << hitboxLayer);
                    RaycastHit[] attackHits = Physics.BoxCastAll(transform.position, new Vector3(0.5f, 0.5f, 0.5f), new Vector3(0.0f, 0.0f, 1.0f), transform.rotation, 1.0f, mask);
                    foreach (RaycastHit hit in attackHits)
                    {
                        Hitbox hitBox = hit.collider.GetComponent<Hitbox>();
                        if (hitBox)
                        {
                            Assert.IsNotNull(hitBox.m_Owner);
                            if (hitBox.m_Owner.tag == "player")
                            {
                                hitBox.sv_ReceiveDamage(damagePerAttack);
                                break;
                            }
                        }
                    }
                    targetInRange = false;
                    timeUntilNextAttack = timeBetweenAttacks;
                }
            }
        }
        delayRemaining -= Time.deltaTime;
        timeUntilNextAttack -= Time.deltaTime;
    }

    //void OnTriggerEnter(Collider collider)
    //{
    //    if (collider.tag == "player")
    //    {
    //        target = collider.gameObject;
    //        timeUntilNextAttack = attackDelay;
    //    }
    //}

    //void OnTriggerExit(Collider collider)
    //{
    //    if (collider.tag == "player")
    //    {
    //        target = null;
    //    }
    //}
}
